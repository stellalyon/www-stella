{% set title = 'Blog − Au bout d’un projet libre : l’idée' %}
{% set description = 'Lancer un projet libre ? Vous y avez forcément déjà pensé, et vous avez
        peut-être même déjà créé un dépôt avec un peu de code dedans.
        Évidemment, après quelques semaines d’une passion fiévreuse, il est un
        peu tombé dans l’oubli… Comment font tous ces gens qui y arrivent ?' %}
{% set image = url_for('static', filename='images/00017-image.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>Au bout d’un projet libre : l’idée (1 / 4)</h2>
      <aside><time datetime="2020-10-15">15 octobre 2020</time>, par Guillaume</aside>
      <p>
        Lancer un projet libre ? Vous y avez forcément déjà pensé, et vous avez
        peut-être même déjà créé un dépôt avec un peu de code dedans.
        Évidemment, après quelques semaines d’une passion fiévreuse, il est un
        peu tombé dans l’oubli… Comment font tous ces gens qui y arrivent ?
      </p>
    </header>

    {% include 'articles/_libre_contents.jinja2' %}

    <section>
      <h3>Un projet libre, pour quoi faire ?</h3>
      <p>
        Créer un projet libre, avec des utilisateurs et une petite communauté,
        c’est difficile. On a beau mettre toute son énergie et toute son envie,
        on arrive rarement à ses fins.
      </p>
      <p>
        Déjà… Arriver à dépasser l’esquisse, le fichier de quelques dizaines ou
        centaines de lignes, c’est une étape qui peut sembler inaccessible. Et
        puis, pour être parfaitement franc : démarrer, déjà, c’est dur.
      </p>
      <p>
        Toutes les raisons sont bonnes pour ne pas aller au bout : ce logiciel
        est nul, il ne sert à rien, je ne sais pas coder, les autres codent
        mieux que moi, je n’ai jamais fait de libre, je n’ai pas de temps, j’ai
        déjà tout ce qu’il me faut… On les connait, ces excuses, et pour
        cause : on les a toutes déjà utilisées personnellement.
      </p>

      <h4>L’envie</h4>
      <p>
        Là, c’est différent. Vous avez en vous l’envie d’aller au bout. Quelque
        part en vous, vous le sentez : c’est le moment.
      </p>
      <p>
        L’envie de faire un projet libre, c’est un peu la condition sine qua
        non pour faire ce projet libre. Ça peut paraître évident, mais ça ne
        l’est pas forcément quand on y regarde de plus près. Il arrive souvent
        d’écrire du code parce qu’on y est contraint, que ce soit un petit
        script pour automatiser quelque chose sur son ordinateur ou pour
        corriger un bug dans un programme qu’on utilise. Sous contrainte, ce
        n’est sans doute pas la peine de se lancer dans quelque chose de
        durable. Ça ne fonctionnera pas.
      </p>
      <p>
        L’envie, c’est ce qu’il vous restera quand il ne vous restera plus
        rien. Et au début, on n’a souvent que cela. L’envie. Alors partez de
        cela, c’est déjà très bien.
      </p>
      <p>
        L’envie, ce n’est ni la compétence, ni la légitimité, ni le
        besoin. L’envie, c’est quelque chose qui doit dépasser les peurs et les
        craintes. Elle n’a pas besoin de justification. Laissez donc ces doutes
        loin de votre tête, personne n’est là pour vous regarder pour
        l’instant. Cette arme est suffisante, vous n’avez pas besoin d’autre
        chose, et ceux qui vous diront le contraire seront des oiseaux de
        mauvais augure. Vous y compris.
      </p>
      <p>
        Oui, c’est un travail qui peut être considérable pour certaines et pour
        certains. Mais ne vous inquiétez pas : nous sommes entre nous. Vous
        pouvez avoir confiance en vous.
      </p>

      <h4>L’idée</h4>
      <figure>
        <img src="{{ url_for('static', filename='images/00017-xkcd.png')}}" alt="Mandatory Related XKCD™" title="We didn't believe you at first, but we asked like three people who were at that party. They not only corroborated your story, but even said you totally mentioned wanting to start a company someday. Sorry! If this isn't enough money, let us know." />
        <figcaption>
          Le <a href="https://xkcd.com/827/">XKCD</a> obligatoire. Vous avez
          eu toutes ces idées avant tout le monde. Un système d’exploitation
          libre ? Votre idée. Une suite bureautique libre ? Votre idée
          aussi. C’est insupportable, tous ces gens qui volent ce qui traîne
          dans votre tête.
        </figcaption>
      </figure>
      <p>
        L’idée, c’est la grande star du storytelling. « Je me suis réveillé un
        matin de juillet, et je me suis dit, en regardant la tranquille mer
        monégasque s’étendre à l’horizon, que créer mon entreprise de location
        de yachts en or massif était l’idée disruptive qui allait fonder ma
        fortune ». Gros plan sur la chevelure d’un trentenaire en chemise un
        peu ouverte. « Ce n’est pas donné à tout le monde d’avoir une idée
        brillante comme ça. C’est pour ça que je suis millionaire. Une idée
        comme ça, c’est du travail, vous savez. Je me suis fait tout seul, sans
        personne, à part ma mère banquière d’affaires qui m’a filé un petit
        coup de main au début ».
      </p>
      <p>
        Vous voulez un avis personnel tranché ? L’idée ne sert à rien. Vous
        n’avez pas d’idée de projet ? Tant mieux. On va faire sans.
      </p>
      <p>
        Des projets qui manquent, à vous, à d’autres, il y en a des pelles, des
        montagnes, des torrents. Tous ces projets propriétaires qui n’existent
        pas en libre. Tous ces projets libres dont l’interface vous
        gonfle. Tous ces projets en ligne de commande qui seraient mieux avec
        une fenêtre et quelques boutons. Tous ces projets en ligne qui seraient
        mieux avec un client lourd. Tous ces clients lourds qui seraient mieux
        en ligne.
      </p>
      <p>
        Et plus généralement : tous ces logiciels sur lesquels vous aimeriez
        changer deux ou trois trucs.
      </p>
      <p>
        Bien sûr, vous pouvez également contribuer à des projets libres
        existants. Mais si vous sentez que vous feriez tout un peu autrement,
        si vous avez l’impression que l’organisation globale de cette interface
        mériterait d’être revue de fond en comble, en bref : si l’envie est
        trop forte, alors laissez-là vous envahir. Créez un projet.
      </p>
      <p>
        Créer un projet, c’est souvent prendre les bonnes idées des autres et y
        ajouter un petit grain de sel personnel. Pas besoin de tout réinventer,
        parce qu’une bonne partie existe déjà. Vous pouvez même reprendre du
        code si la licence vous le permet. C’est fait pour. C’est pour ça que
        le libre est libre : il vous permet d’apprendre en copiant. Il n’y a
        pas de honte à cela.
      </p>
      <p>
        Allez, on commence.
      </p>
    </section>

    <section>
      <h3>Le grand secret</h3>
      <figure>
        <img src="{{ url_for('static', filename='images/00017-secret.jpg')}}" alt="Une bibliothèque avec un passage secret" title="On se croirait un peu dans Da Vinci Code, non ? Ou dans Harry Potter." />
        <figcaption>
          C’est un secret qui se transmet de génération en génération. Il reste
          à l’abri de l’air et de la lumière dans cette grande cave derrière de
          vieux grimoires que mes ancêtres, et leurs ancêtres avant eux, ont
          fait semblant de lire pour se donner un style dans les soirées mousse
          entre amis. Celles qu’on ne peut plus faire à cause de
          vous-savez-quoi. Mouais. Je vais peut-être lire tout ça, finalement.
        </figcaption>
      </figure>
      <p>
        Comment savoir si notre projet est un bon projet ? Pas facile…
        Pourtant, il existe une technique secrète qui peut vous donner quelques
        indications sur le degré de coolitude de votre création.
      </p>
      <p>
        Cette technique secrète, c’est de répondre à la question « Quand est-ce
        que vous utiliserez régulièrement votre projet ? » Si la réponse est
        « Dans pas très longtemps », alors vous partez sur une base carrément
        solide. Et sinon, il va falloir réfléchir un peu plus.
      </p>
      <p>
        Pourquoi cette technique fonctionne ? Parce que si vous utilisez votre
        logiciel rapidement, vous vous assurez d’un seul coup :
      </p>
      <ol>
        <li>que c’est utile ;</li>
        <li>que vous aurez envie de l’améliorer, assez pour qu’il soit sympa à utiliser ;</li>
        <li>que vous corrigerez les bugs les plus importants ;</li>
        <li>que vous aurez un intérêt à le maintenir.</li>
      </ol>
      <p>
        Toutes ces raisons-là sont des indicateurs assez pertinents du
        développement et de la longévité d’un projet.
      </p>
      <p>
        Voilà, c’est tout.
      </p>
      <p>
        Si votre réponse est un peu différente ? Alors là, ça dépend. Si vous
        pensez que vous l’utiliserez dans longtemps, parce que cela nécessite
        beaucoup, beaucoup de travail avant d’être utilisable, alors il vous
        faudra de la patience. Si vous avez peur d’utiliser un autre logiciel
        par facilité, même après avoir écrit un logiciel utilisable (mais moins
        bien), il vous faudra un peu de discipline. Si vous pensez que vous ne
        l’utiliserez qu’une fois, alors il n’a peut-être pas intérêt à devenir
        un projet libre utilisable par tous et maintenu.
      </p>
      <p>
        Enfin, si vous ne l’utiliserez jamais, alors c’est autre chose qu’un
        projet libre. Ce peut être une bonne chose pour apprendre, pour se
        former, pour découvrir. Ce peut être un essai, une tentative qui finira
        vite dans la corbeille. Mais ça risque de ne jamais être un vrai projet
        libre.
      </p>
    </section>

    <section>
      <h3>L’aventure</h3>
      <p>
        Lancez-vous dans cette aventure dont vous êtes l’héroïne ou le
        héros. Pour toute aventure il y a les premiers pas, les premières
        lignes. Tous les logiciels ont commencé comme cela.
      </p>
      <p>
        Ce n’est pas la peine de réfléchir à une analyse sociologique ou
        scientifique de votre projet. La tentation est grande, même après avoir
        passé avec succès l’épreuve ardue du grand secret, de trouver de bonnes
        raisons de se laisser gagner par la procrastination. Et pour cela, vous
        rencontrerez la tentation de questions plus métaphysiques les unes que
        les autres. Qui suis-je pour me permettre de créer ? Que vaux-je par
        rapport à toutes ces créations géniales ? À quoi sert-il de refaire un
        projet déjà fait mille fois ? Ma contribution sera-t-elle réellement
        déterminante dans l’histoire de l’humanité ? De l’univers ? D’ailleurs,
        l’univers existe-t-il réellement ?
      </p>
      <p>
        Laissez les autres répondre à ces questions pour vous. Vous avez plus
        important à faire : créer un projet.
      </p>
      <p>
        Ça y est. Vous avez envie de créer un dépôt et de vous jeter sur le
        code. Halte là ! Ça vaut le coup de se pencher sur quelques questions
        techniques auparavant. Et comme la vie est bien faite, c’est le sujet
        de <a href="{{ url_for('blog', article='00019-au-bout-d-un-projet-libre-le-plan') }}">notre prochain article</a>…
      </p>
    </section>
  </article>

{% endblock content %}
