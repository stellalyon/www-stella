{% set title = 'Blog − Il faut sauver le web' %}
{% set description = 'D’accord, le titre est un brin dramatique, mais il n’empêche : l’heure est grave dans le monde des navigateurs. Imperceptiblement, chaque utilisateur se fait déposséder d’un bien qui devrait être un peu le sien. Et si vous êtes là, à lire cet article, c’est que l’utilisateur, c’est vous.' %}
{% set image = url_for('static', filename='images/00035-web.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>Il faut sauver le web</h2>
      <aside><time datetime="2021-09-30">30 septembre 2021</time>, par Guillaume</aside>
      <p>
        D’accord, le titre est un brin dramatique, mais il n’empêche : l’heure
        est grave dans le monde des navigateurs. Imperceptiblement, chaque
        utilisateur se fait déposséder d’un bien qui devrait être un peu le
        sien. Et si vous êtes là, à lire cet article, c’est que l’utilisateur,
        c’est vous.
      </p>
    </header>

    <section>
      <h3>Quand David devient Goliath</h3>

      <p>
        Le web, c’était mieux avant.
      </p>

      <p>
        Évidemment, ce n’est pas vrai. Ne commencez pas à me traiter de gros
        réac, j’exagère un peu, je le sais bien. Pour ne pas oublier la peine
        qui fut celle de nos ancêtres du XX<sup>e</sup> siècle, pour ne pas
        sombrer dans une aveuglante nostalgie primaire, voici un aperçu du web
        pré-2000.
      </p>

      <figure>
        <img src="{{ url_for('static', filename='images/00035-amazon.webp') }}" alt="Site d’Amazon en 1995" title="Ti duuuu, ti duuuuuuu (c’est le bruit du modem)" />
        <figcaption>
          <p>
            Amazon, 1995. C’est moche, on est d’accord. Et pour arriver ici
            c’était coton, imaginez que Google n’existait pas. Cela dit, moins
            de 100 000 sites web étaient en ligne cette année-là dans le monde
            entier, alors…
          </p>
          <p>
            (D’autres images sont à découvrir sur l’excellent
            <a href="https://www.versionmuseum.com/">Version Museum</a>.)
          </p>
        </figcaption>
      </figure>

      <p>
        Amazon, à l’époque, n’est guère plus qu’une librairie. Quoi de plus
        inoffensif qu’une librairie ? Quoi de plus beau que de répandre la
        lecture et le savoir à petit prix ? « Un million de titres, des prix
        constamment bas. » Et en même temps, « Earth’s biggest bookstore ».
      </p>

      <p>
        C’est cette même entreprise qui aujourd’hui est accusée de tant de
        maux : détruire la petite librairie de quartier, paupériser les
        commerces de proximité, ringardiser la grande distribution tout en
        sacrifiant ses emplois, américaniser la culture mondiale, et même
        <a href="https://www.franceculture.fr/emissions/revue-de-presse-internationale/la-revue-de-presse-internationale-emission-du-vendredi-26-mars-2021">pousser ses salariés à uriner dans des bouteilles</a>
        pour maintenir les cadences infernales de livraisons.
      </p>

      <p>
        Que s’est-il passé entretemps ? C’est simple : Amazon a grandi. Amazon
        est devenu énorme. Amazon est devenu un ogre. Et il a tout bouffé.
      </p>

      <p>
        Cette histoire, vous l’avez entendue mille fois. La petite startup, le
        mec dans un garage qui mange des pizzas, les nuits passées devant un
        écran cathodique d’ordinateur, la croissance exponentielle, les
        bienfaits sur l’économie, l’emploi, la science, la culture…
      </p>

      <p>
        Cette histoire est belle. Elle est
        <a href="https://fr.wikipedia.org/wiki/Biais_des_survivants">incroyablement biaisée</a>,
        mais elle est belle, et elle fait rêver. Et pour les
        anarcho-communistes les plus récalcitrants, on use des
        <a href="https://fr.wikipedia.org/wiki/Licorne_(économie)">artifices lexicaux</a> de
        rigueur pour incruster cette idée au plus profond des esprits. Cette
        histoire est belle, un point c’est tout.
      </p>

      <figure>
        <img src="{{ url_for('static', filename='images/00035-survivor.svg') }}" alt="Les endroits endommagés des avions revenus du front" title="Nos ennemis sont tellement bêtes qu’ils ne touchent jamais nos réacteurs…" />
        <figcaption>
          L’exemple canonique du biais des survivants. Afin d’améliorer la
          solidité des avions, il est important de les blinder là où ils
          <strong>n’ont pas été touchés</strong> : ce n’est pas que les avions
          sont toujours touchés ici, c’est surtout que ceux qui ne reviennent
          pas sont touchés ailleurs. C’est tellement contre-intuitif qu’il a
          fallu un grand statisticien, Abraham Wald, pour s’en rendre compte.
          Cela dit, maintenant qu’on le sait, ça n’empêche toujours pas
          certains médias de relayer aveuglément les discours méritocratiques
          des géants du web.
        </figcaption>
      </figure>

      <p>
        Amazon n’est pas seul. Beaucoup des géants de l’informatique aiment mettre
        en avant
        <a href="https://about.google/intl/fr_fr/our-story/">leurs débuts modestes</a>
        et <a href="https://news.microsoft.com/about/">leurs ascensions délirantes</a>.
        Les géants sont gros, certes, mais ils ont été petits avant. Ils
        doivent leurs astronomiques quantités d’argent au fait d’avoir su
        servir le client. Ils ont changé le monde, et si ça a fonctionné, c’est
        que le monde avait besoin d’eux. Ils le méritent, parce
        qu’<a href="https://www.apple.com/privacy/">ils sont du côté de l’utilisateur</a>.
      </p>

      <p>
        Cette histoire nous berce… Mais jusqu’où ?
      </p>
    </section>

    <section>
      <h3>L’utopie</h3>

      <p>
        Cette histoire de startup, ce n’est pas la seule histoire matricielle
        qui berce l’informatique, Internet, le web. Très tôt, des voix se sont
        fait entendre pour dénoncer l’intérêt vorace naissant des états. La
        <a href="https://fr.wikipedia.org/wiki/Déclaration_d'indépendance_du_cyberespace">déclaration d’indépendance du cyberespace</a>,
        avec la naissance de l’<a href="https://www.eff.org/about">EFF</a>, est
        un jalon fondateur de cette prise de conscience.
      </p>

      <p>
        Les états sont voraces, les entreprises aussi. L’inexorable montée
        d’une marchandisation du web fait grincer des dents, par exemple
        <a href="http://www.uzine.net/article60.html">en France chez certains pionniers de la toile</a>.
      </p>

      <p>
        Parce qu’à la base, le web n’est pas franchement un truc de
        capitalistes.
        <a href="https://fr.wikipedia.org/wiki/World_Wide_Web#Histoire">Créé par des chercheurs, volontairement mis dans le domaine public</a>,
        le web et ses outils sont loin de faire rêver les personnes à la
        recherche de rentabilité rapide. Un autre discours, dominant lorsque
        personne ne s’y intéressait, présentait alors le web comme
        <a href="https://www.franceculture.fr/conferences/aux-sources-de-l-utopie-numerique-de-la-contre-culture-la-cyberculture">la mise dans le cyberespace d’une communauté hippie</a>.
        Sans frontière, sans interdit, sans instance centrale, il devait nous
        offrir un espace de liberté et nous transformer en acteurs plutôt qu’en
        consommateurs.
      </p>

      <figure>
        <img src="{{ url_for('static', filename='images/00035-microsoft.jpg') }}" alt="Photo des employés de Microsoft en 1975" title="Cheese!" />
        <figcaption>
          Cheveux longs, lunettes teintées et barbes fournies… On n’est pas à
          Woodstock, mais bien sur la photo de groupe des premiers employés de
          Microsoft, à la fin des années 1970.
        </figcaption>
      </figure>

      <p>
        Cet espoir d’émancipation n’est pas qu’une vue de l’esprit.
        <a href="https://www.letemps.ch/opinions/quest-devenu-linternet-libertaire-debut">De l’aveu même de son « inventeur »</a>,
        le web avait dans son idée fondatrice la graine de l’horizontalité
        heureuse. L’outil, placé dans les mains d’un peuple éclairé, devait
        l’emmener vers la créativité et la liberté.
      </p>

      <p>
        D’ailleurs, l’opposition entre hippies libertaires et capitalistes
        sanglants est particulièrement factice ici : ce sont souvent les mêmes
        personnes. On peut, par exemple, citer l’attrait public de
        <a href="https://en.wikipedia.org/wiki/Steve_Jobs">Steve Jobs</a>
        pour le LSD, et l’importance qu’il donne au mysticisme dans son
        parcours de vie.
      </p>

      <p>
        Et Steve Jobs n’est pas le seul : on peut se délecter des « aveux » de
        Bill Gates sur ses expériences de jeunesse dans Playboy, des positions
        ouvertement progressistes de Richard Branson sur la drogue, ou des
        incomparables frasques libertariennes d’Elon Musk. On comprend que les
        mêmes personnes ont pu naviguer entre les deux idéaux selon l’évolution
        de leur situation personnelle.
      </p>

      <p>
        Cette proximité est visible encore plus profondément : ayant grandi
        dans une époque et une ambiance propices à la mise en commun, les
        créateurs de ces géants du web ont assouvi une certaine volonté de
        faire communauté. Le terme même de « communauté » a été largement
        <a href="https://about.instagram.com/fr-fr/community">adopté sur les grande plateformes numériques</a>,
        tout comme il était revendiqué au sein des communautés hippies. Peut-on
        dès lors être surpris d’entendre les streamers parler de « la commu »
        sur Twitch (propriété d’Amazon), là où le terme évoquait il y a
        quelques décennies les kolkhozes, les kibboutz, ou la commune de
        Paris ?
      </p>

      <p>
        Peut-être que le « web d’avant » est mort simplement parce que ses
        glorieux inventeurs ont accompli leurs rêves et évolué dans leurs
        valeurs. Mais alors, de quoi faudrait-il le « sauver » ? Faudrait-il
        aller contre l’avis de ses têtes pensantes originales qui ont le mieux
        réussi professionnellement ?
      </p>
    </section>

    <section>
      <h3>Noir c’est noir</h3>

      <p>
        De quoi faudrait-il sauver le web ?
      </p>

      <p>
        Après tout, on a le droit de réussir professionnellement, même
        lorsqu’il faut pour cela s’écarter de certains de ses principes. Non
        contents de changer le monde, ces mastodontes l’on fait en créant de la
        richesse, des emplois, du progrès technique. De quoi se plaint-on ?
      </p>

      <p>
        Nous pourrions bien sûr parler longtemps de
        <a href="https://www.huffingtonpost.fr/entry/publicite-intrusive-proposition-loi_fr_5dc43d03e4b00551388466fc">dérives publicitaires</a>,
        de <a href="https://fr.wikipedia.org/wiki/Cambridge_Analytica">surveillance et fichage systématiques</a>,
        de <a href="https://www.france.tv/france-2/complement-d-enquete/2721737-fake-news-la-machine-a-fric.html">désinformation enrichissante</a>.
        Nous pourrions également rétorquer que nombre de ces licornes ont largement
        <a href="https://www.nextinpact.com/article/5656/79247-une-taxe-sur-bande-passante-plutot-que-sur-donnees-personnelles">bénéficié de réseaux et d’infrastructures publics</a>,
        pour au final devenir maîtres de
        « <a href="https://www.francetvinfo.fr/internet/amazon/video-jeff-bezos-patron-d-amazon-et-champion-de-l-optimisation-fiscale_4379459.html">l’optimisation fiscale</a> ».
      </p>

      <p>
        Mais voyons plus large. Au-delà des affinités politiques de chacun,
        certains comportements font unanimement grincer des dents. Lorsque l’on
        parle d’abus de position dominante et de monopole, même les très
        libéraux États-Unis sont
        <a href="https://en.wikipedia.org/wiki/United_States_v._Microsoft_Corp.">capables de sévir</a>.
        Ces situations ne sont pas seulement déplorables pour les citoyens,
        elles le sont aussi à long terme pour l’économie. Dès lors, des voix
        différentes s’élèvent, et remontent jusqu’aux instances judiciaires.
      </p>

      <figure>
        <img src="{{ url_for('static', filename='images/00035-chrome.png') }}" alt="Publicité pour Chrome" title="Pourquoi vous passer de nous alors que vous pourriez encore plus être dépendants de nous ?" />
        <figcaption>
          Google n’a pas trop hésité à abuser de sa position dominante sur les
          moteurs de recherche pour pousser les gens à utiliser Chrome, leur
          propre navigateur, via de la publicité sur la page la plus vue au
          monde.
        </figcaption>
      </figure>

      <p>
        Maintenant, pour ces grandes entreprises, il n’est plus seulement
        important d’utiliser le web, qu’elles se sont réparti comme un gâteau.
        Il est surtout question de
        <strong>transformer le web pour ses intérêts propres</strong>. Il faut
        inlassablement éroder le pouvoir de l’utilisateur pour le réduire à
        peau de chagrin, et asseoir son propre agenda. Pas forcément pour le
        bien public, comme nous le verrons plus tard.
      </p>

      <p>
        C’est de cela qu’il faut sauver le web.
      </p>

      <p>
        Ce n’était pas mieux avant. Mais à bien y réfléchir, peut-être est-ce
        pire après.
      </p>

      <p>
        Voilà. C’en est enfin fini de l’introduction. Vous trouviez que le
        titre de l’article était dramatique ? Attendez donc de lire
        <a href="{{ url_for('blog', article='00037-adieu-diversite-du-web') }}">la suite</a>.
      </p>

      <p>
        <em>À suivre…</em>
      </p>
    </section>
  </article>

{% endblock content %}
