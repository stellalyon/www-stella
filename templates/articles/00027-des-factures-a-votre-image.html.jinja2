{% set title = 'Blog − Des factures à votre image' %}
{% set description = 'Créer des factures, c’est assez facile. Mais comment faire pour avoir de belles factures à votre image, qui s’adaptent automatiquement au contenu ?' %}
{% set image = url_for('static', filename='images/00027-facture-sloop.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>Des factures à votre image</h2>
      <aside><time datetime="2021-03-25">25 mars 2021</time>, par Stella et <a href="https://madcats.fr/">Madcats</a></aside>
      <p>
        Créer des factures, c’est assez facile. Mais comment faire pour avoir
        de belles factures à votre image, qui s’adaptent automatiquement au
        contenu ?
      </p>
    </header>

    {% include 'articles/_documents_contents.jinja2' %}

    <section>
      <h3>Un modèle de facture</h3>

      <p>
        La création de factures est un grand classique de la création de
        documents en entreprise. Que vous ayez besoin de les écrire à la main
        ou que vous ayez une plateforme de vente en ligne, vous avez besoin de
        les générer régulièrement.
      </p>
      <p>
        La mise en page d’un modèle de facture peut être plus compliquée qu’il
        n’y paraît. Si le type de contenu à inclure est toujours similaire, une
        partie importante des données varie cependant : l’adresse du client,
        les produits vendus, les montants et les quantités… Est-on sûr du
        résultat quand le nombre de lignes dans le tableau devient très
        important, ou quand la description des opérations à faire s’allonge
        inexorablement ?
      </p>
      <p>
        Et bien sûr, on souhaite que la facture suive au maximum la
        charte graphique de l’entreprise. Il faut donc y inclure des logos, des
        couleurs, des fontes qui correspondent à l’image que l’on veut donner.
      </p>
      <p>
        Pour les factures, comme pour la plupart des documents institutionnels,
        il est généralement possible de créer un modèle dans un traitement de
        texte, puis de le copier et de le modifier à chaque fois. Mais on
        s’expose à ce moment-là à l’inévitable erreur de copier-coller, ou aux
        erreurs de calcul pour les sommes indiquées.
      </p>
      <p>
        <strong>
          L’automatisation est une solution idéale, autant pour le contenu que
          pour la mise en page.
        </strong>
        C’est une bonne manière de satisfaire toutes les parties prenantes : le
        client, bien entendu, mais aussi les graphistes (qui aiment une mise en
        page parfaite), les responsables juridiques (qui veulent que tous les
        champs requis soient toujours intégrés), les équipes commerciales (qui
        veulent facilement et rapidement créer les factures).
      </p>
      <p>
        Comment faire, et pour quel résultat ? C’est ce que nous allons voir.
      </p>
    </section>

    <section>
      <h3>Les sous et les couleurs</h3>

      <p>
        Le casse-tête commence d’un point de vue graphique. À partir d’un
        contenu validé par la direction, on souhaiterait avoir une grande
        liberté créative pour donner envie au client de payer avec une facture
        engageante (si, je vous assure, certains clients sont contents de
        payer, surtout si la facture est agréable à regarder). La facture étant
        par définition un document qui s’archive, elle est également l’une des
        images que le client gardera du prestataire. Alors, autant investir un
        peu de temps pour l’utiliser comme outil de communication.
      </p>
      <p>
        Sans chercher à être exhaustif avec les mentions légales nécessaires
        (vos conseillers juridiques s’en chargeront), voici ce à quoi pourrait
        ressembler votre première facture.
      </p>
      <figure>
        <a href="https://gitlab.com/stellalyon/weasyprint-examples/-/raw/master/invoice/sober/invoice.pdf?inline=false">
          <img src="{{ url_for('static', filename='images/00027-facture-sober.jpg')}}" alt="Facture simple" title="Facture simple" />
        </a>
        <figcaption>
          Une facture (très) simple mais efficace.
        </figcaption>
      </figure>
      <p>
        Cette facture est particulièrement simple. Elle ne se démarque pas
        franchement d’un point de vue graphique, mais elle est au moins claire
        et lisible.
      </p>
      <p>
        Vous vous dites que n’importe qui aurait pu la faire avec un vulgaire
        tableur (et un peu de patience pour régler tous les alignements des ces
        fichues cellules qui ne rentrent jamais vraiment sur une page). Vous
        n’avez pas tort.
        <strong>
          Mais cette facture simple est faite différemment, et elle a un
          super-pouvoir : elle peut changer d’apparence en un clin d’œil.
        </strong>
      </p>
      <p>
        Ah bon ? Totalement ! Alors, à quoi pourrait-elle donc ressembler ?
      </p>
      <figure>
        <a href="https://gitlab.com/stellalyon/weasyprint-examples/-/raw/master/invoice/nae/invoice.pdf?inline=false">
          <img style="width: 22%" src="{{ url_for('static', filename='images/00027-facture-nae.jpg')}}" alt="Modèle de facture Nae" title="Modèle de facture Nae" />
        </a>
        <a href="https://gitlab.com/stellalyon/weasyprint-examples/-/raw/master/invoice/ntea/invoice.pdf?inline=false">
          <img style="width: 22%" src="{{ url_for('static', filename='images/00027-facture-ntea.jpg')}}" alt="Modèle de facture Ntea" title="Modèle de facture Ntea" />
        </a>
        <a href="https://gitlab.com/stellalyon/weasyprint-examples/-/raw/master/invoice/skin/invoice.pdf?inline=false">
          <img style="width: 22%" src="{{ url_for('static', filename='images/00027-facture-skin.jpg')}}" alt="Modèle de facture Skin" title="Modèle de facture Skin" />
        </a>
        <a href="https://gitlab.com/stellalyon/weasyprint-examples/-/raw/master/invoice/sloop/invoice.pdf?inline=false">
          <img style="width: 22%" src="{{ url_for('static', filename='images/00027-facture-sloop.jpg')}}" alt="Modèle de facture Sloop" title="Modèle de facture Sloop" />
        </a>
        <figcaption>
          Un seul contenu, mais quatre nouvelles apparences. Sans toucher au
          modèle de base.
        </figcaption>
      </figure>
      <p>
        Attendez… À partir de la même facture, on peut faire tout ça sans changer
        le contenu ?
      </p>
      <p>
        Oui. Ce n’est pas de la magie, c’est juste un petit peu de technique.
        Mais… Comment ça marche ?
      </p>
    </section>

    <section>
      <h3>Je thème</h3>

      <p>
        Rassurez-vous, nous n’irons pas nous perdre dans les tréfonds des
        solutions techniques. Nous survolerons juste les quelques petites
        choses qu’il faut pour comprendre un minimum ce qui se passe.
      </p>
      <p>
        À bien y réfléchir, cette manière de décliner du contenu avec des
        apparences différentes est assez commune avec un outil que nous
        utilisons tous les jours : Internet. Vous avez déjà vu des tonnes de
        blogs, dont le type de contenu est toujours le même (une liste
        d’articles, dans les grandes lignes). Et pourtant, chaque blog à son
        apparence bien à lui.
      </p>
      <p>
        Depuis de nombreuses années, un site montre assez bien ce
        fonctionnement :
        <a href="http://www.csszengarden.com/">CSS Zen Garden</a>. Le même
        contenu y est présenté avec de nombreux styles graphiques. Vous devriez
        y trouver quelque chose à votre goût, que vous aimiez
        <a href="http://www.csszengarden.com/217/">les grosses lettres et aplats de couleurs</a>,
        <a href="http://www.csszengarden.com/220/">les présentations épurées</a>,
        <a href="http://www.csszengarden.com/211/">les orchidées</a> ou
        <a href="http://www.csszengarden.com/213/">les voyages sous la mer</a>.
      </p>
      <p>
        <strong>
          Nos factures utilisent simplement la même technologie que les sites
          Internet.
        </strong>
      </p>
      <p>
        Avec HTML et CSS (dont vous avez sans doute déjà entendu parler), à
        partir d’un seul contenu, il est possible de changer la présentation
        pour obtenir ce que l’on souhaite, avec une grande liberté de création.
      </p>
      <p>
        Cette solution offre de grands avantages. Comme pour les sites, il est
        possible de garder une partie fixe qui ne changera pas d’une facture à
        l’autre (les informations légales, en particulier) et de décider
        quelles parties seront variables et modifiables par l’utilisateur. Il
        est même possible d’automatiser les sommes, histoire d’éviter les
        erreurs de calcul qui sont toujours un peu gênantes quand le client
        s’en rend compte (c’est arrivé à tout le monde, ce n’est pas la peine
        de nier).
      </p>
      <p>
        Pour la partie graphique, c’est également une libération : demandez aux
        graphistes autour de vous s’ils aiment créer quelque chose de joli sur
        Word, ou s’ils préfèrent le faire sur un site Internet (spoiler alert :
        ils préfèrent Internet). Les technologies du Web apportent une liberté
        inégalée, et permettront d’obtenir un résultat de meilleure qualité en
        moins de temps, peut-être même avec le sourire de tout le monde.
      </p>
    </section>

    <section>
      <h3>Et après ?</h3>

      <p>
        Il y aurait beaucoup d’autres choses à dire sur les factures, leurs
        tableaux qui peuvent devenir trop longs, leurs descriptions qui peuvent
        prendre un peu trop de place, leurs subtilités dans l’affichage des
        cellules de tableaux… On voit la qualité des documents dans ces
        détails, et il ne faudrait rien laisser au hasard pour arriver à
        produire les factures parfaites que vos clients paieront avec le
        sourire 😁.
      </p>
      <p>
        Patience, patience… Nous parlerons de tout cela dans le prochain
        article !
      </p>
    </section>
  </article>

{% endblock content %}
