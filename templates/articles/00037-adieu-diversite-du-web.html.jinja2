{% set title = 'Blog − Adieu, diversité du web' %}
{% set description = 'Vous lisez cet article en utilisant Google Chrome ? C’est statistiquement hautement probable. Et si ce n’est pas le cas, ne vous inquiétez pas, vous n’aurez bientôt plus le choix…' %}
{% set image = url_for('static', filename='images/00037-edge-chromium.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>Adieu, diversité du web</h2>
      <aside><time datetime="2021-11-04">4 novembre 2021</time>, par Guillaume</aside>
      <p>
        Vous lisez cet article en utilisant Google Chrome ? C’est
        statistiquement hautement probable. Et si ce n’est pas le cas, ne vous
        inquiétez pas, vous n’aurez bientôt plus le choix…
      </p>
    </header>

    <section>
      <h3>La victoire des standards</h3>

      <p>
        Pour celles et ceux d’entre vous qui ont grandi lors du siècle dernier,
        le nom d’Internet Explorer peut, au choix, vous rappeler une petite
        <a href="https://fr.wikipedia.org/wiki/Internet_Explorer#/media/Fichier:Internet-explorer.png">icône bleue</a>
        sur laquelle vous double-cliquiez pour « aller surfer sur l’Internet »,
        ou vous remémorer des heures de détresse (je pèse mes mots) à
        développer un site qui fonctionne à peu près sur cette création
        logicielle parfaitement maléfique.
      </p>

      <p>
        Pour celles et ceux d’entre vous auxquels cela n’évoque rien du tout,
        voici un très court cours d’histoire : nous sommes en l’an 2000, et le
        web commence doucement à s’inviter sur bon nombre d’ordinateurs
        individuels à travers le monde. Pour surfer, un outil est très
        majoritairement utilisé : Internet Explorer, de Microsoft. C’est un
        outil assez pratique à utiliser, mais il traîne une sulfureuse
        réputation : celle d’afficher les sites Internet n’importe comment. Et
        ce n’est pas qu’une rumeur : personne ne sait exactement comment il
        fonctionne, et créer des sites requiert des heures de tests fastidieux
        (et de magie noire) pour obtenir un résultat aléatoire.
      </p>

      <p>
        <a href="https://fr.wikipedia.org/wiki/Parts_de_marché_des_navigateurs_web">Internet Explorer atteindra en 2004 plus de 95 % de part de marché</a>,
        et écrabouillera son concurrent d’alors, Netscape. À ce moment-là,
        Microsoft est tout puissant et ne travaille pas réellement à améliorer
        son outil : Internet Explorer 6 est sorti en 2001, et il restera en
        l’état pendant 5 longues années, sans réelle évolution, avec ses bugs
        et ses limites. Après tout, pourquoi changer, alors que tous les grands
        sites du monde sont déjà « optimisés pour Internet Explorer » ?
      </p>

      <p>
        Rien ne peut arriver à Microsoft. Les utilisateurs sont captifs,
        d’autant plus qu’il est extrêmement difficile de créer des sites qui
        fonctionnent bien à la fois sur Internet Explorer et sur les autres
        navigateurs. Et pourtant…
      </p>

      <figure>
        <img src="{{ url_for('static', filename='images/00037-firefox-ie.png') }}" alt="Gâteau envoyé par Microsoft à Mozilla pour la sortie de Firefox 2" title="Salut les gueux ! Gardez la pêche, hein !" />
        <figcaption>
          Gâteau envoyé par Microsoft à Mozilla pour la sortie de Firefox 2 en
          2006. Le premier d’une longue série de gâteaux envoyés entre
          créateurs de navigateurs…
        </figcaption>
      </figure>

      <p>
        Pourtant, en à peine 6 ans, cette part de marché va descendre
        inexorablement sous les 50 %. La faute à deux concurrents de taille :
        Firefox, puis Chrome. Les navigateurs apprennent tant bien que mal à
        coexister, et même si la tâche est ardue elle est rendue possible par
        un détail de taille : des standards.
      </p>

      <p>
        Le <a href="https://www.w3.org/">W3C</a> est en effet un consortium en
        charge de définir certains standards du web, et en particulier HTML et
        CSS. Les grands acteurs, dont Microsoft, Mozilla et Google
        (développeurs des navigateurs Internet Explorer, Firefox et Chrome) y
        discutent ensemble des fonctionnalités à intégrer, de la syntaxe des
        formats, des règles de rendu des pages web. Ces standards sont
        gratuitement consultables, permettant à n’importe qui de créer des
        outils interopérables, compatibles avec les autres.
      </p>

      <p>
        Cette interopérabilité est primordiale dans le développement du web.
        Les navigateurs fleurissent, et ce ne sont pas les seuls : une myriade
        de services utilisent les protocoles, formats et outils utilisés par le
        web. Toutes ces technologies sont testées, détournées, améliorées dans
        un joyeux bazar qui a fait naître une bonne partie de ce qu’Internet a
        aujourd’hui à nous offrir. Cela est possible parce que les outils
        parlent les mêmes langages, et que ces langages sont définis grâce à la
        concertation ouverte de nombreux acteurs.
      </p>

      <p>
        Les standards ont gagné. C’est du moins ce que l’on croyait.
      </p>
    </section>

    <section>
      <h3>Tous contre un, un contre tous</h3>

      <p>
        Ne criez pas victoire trop vite, car la funeste histoire se répète.
      </p>

      <p>
        La grande ascension d’Internet Explorer n’était pas due qu’à
        l’excellence du logiciel (ton sarcastique). Microsoft, en installant
        par défaut son navigateur sur toutes les machines équipées de Windows,
        s’était assuré d’avoir facilement une base d’utilisateurs considérable.
        Il avait ensuite mesquinement utilisé de critiquables astuces pour
        s’assurer une position hégémonique :
        <a href="https://en.wikipedia.org/wiki/Removal_of_Internet_Explorer">impossibilité de désinstaller Internet Explorer</a>,
        <a href="http://blogs.developpeur.org/fremycompany/archive/2007/12/26/les-transformations-directx-sous-internet-explorer-css-filter.aspx">intégration de technologies fermées spécifiques à Windows</a>,
        <a href="https://muffinman.io/blog/ie6-hacks-ten-years-after/">incompatibilités criantes avec les autres acteurs</a>…
      </p>

      <p>
        Aujourd’hui, en 2021, les acteurs ont bien changé… Mais pas la situation.
      </p>

      <p>
        Chrome est devenu l’ogre omniprésent. Sur les ordinateurs, il a environ
        70 % de part de marché. Mais ce n’est pas tout :
        <a href="https://brucelawson.co.uk/2013/hello-blink/">Opera</a> et
        <a href="https://blogs.windows.com/windowsexperience/2018/12/06/microsoft-edge-making-the-web-better-through-more-open-source-collaboration/">Microsoft</a> ont
        abandonné leurs propres moteurs de rendu, et utilisent celui de Chrome
        à la place, faisant d’Opera et Edge de vagues clones de Chrome. Tous
        ensemble, ils dépassent vraisemblablement 85 % des vues sur ordinateur.
        Il n’y a guère que Safari, utilisé ultra-majoritairement par les
        possesseurs d’iPhone
        (<a href="https://developer.apple.com/app-store/review/guidelines/#2.5.6">ont-ils vraiment le choix ?</a>),
        qui résiste avec une vingtaine de pourcents d’utilisateurs mobile.
      </p>

      <figure>
        <img src="{{ url_for('static', filename='images/00037-edge-chromium.jpg') }}" alt="Gâteau envoyé par Google à Microsoft pour la sortie de la version d’Edge basée sur Chromium" title="Merci pour le cadeau, ça vaut bien un gâteau" />
        <figcaption>
          Gâteau envoyé par Google à Microsoft pour la sortie de la version
          d’Edge basée sur Chromium. On se demande qui fait le cadeau à
          l’autre…
        </figcaption>
      </figure>

      <p>
        Pourquoi ? Chrome est un bon navigateur, sans aucun doute. Google a
        investi beaucoup d’énergie, de temps et d’argent pour faire de Chrome
        un excellent outil, bien meilleur que ne pouvait relativement l’être
        Internet Explorer en son temps. Tout le monde s’accorde à ce sujet,
        mais… La réalité n’est-elle pas plus complexe ?
      </p>

      <p>
        Bien sûr que si.
      </p>
    </section>

    <section>
      <h3>Le cheval de Troie technologique</h3>

      <p>
        Tout d’abord, et nous l’avons vu lors de
        <a href="{{ url_for('blog', article='00035-il-faut-sauver-le-web') }}">l’article précédent</a>,
        Google a usé et abusé de publicités vantant les mérites de son
        navigateur. Cette pratique n’est pas en soi frauduleuse, mais lorsque
        la publicité est matraquée sur le site le plus vu du monde, elle
        s’apparente dangereusement à de l’abus de position dominante.
      </p>

      <p>
        Ce n’est pas tout.
      </p>

      <p>
        Google tire beaucoup de ficelles, de manière plus ou moins
        transparente. Régulièrement, Google ajoute de
        <a href="https://en.wikipedia.org/wiki/HTTP/2">nouvelles fonctionnalités</a>
        à Chrome, fonctionnalités qu’il a créées lui-même. Certes, il prend le
        temps de les faire spécifier, afin de rester le chevalier blanc de
        cette belle fable. Mais en réalité, il s’assure à peu de frais une
        supériorité technique en implémentant rapidement dans Chrome ses
        propres technologies. Les autres navigateurs ne peuvent que
        <a href="https://en.wikipedia.org/wiki/HTTP/2#Criticisms">critiquer et suivre</a>…
      </p>

      <p>
        Après tout, pourquoi pas ? Ces technologies ne seront utilisées par les
        créateurs de contenu que si elles sont bonnes, n’est-ce pas ? Eh bien…
        Pas vraiment. Chrome maîtrise
        <a href="https://developers.google.com/speed/pagespeed/insights/?hl=fr">les outils qui définissent la « qualité » des sites</a>.
        Vous n’utilisez pas ce que Google juge bon ? La note de votre site
        baissera. Vous n’avez pas utilisé cette fonctionnalité que Google a
        développée ? Google jugera négativement vos pages.
      </p>

      <p>
        Après tout, pourquoi pas ? Ce n’est qu’une mauvaise note… Certes. Mais
        quand on sait que le jugement de Google sur votre site influence sa
        hauteur dans le référencement, votre choix devient vite cornélien.
        Allez-vous ignorer cette technologie qui ne vous intéresse pas, quitte
        à baisser dans les résultats du moteur de recherche ? Allez-vous
        laisser vos concurrents vous passer devant, s’ils utilisent ces beaux
        joujoux créés par l’ogre américain ?
      </p>

      <p>
        Après tout, pourquoi pas ? Vous aurez votre conscience pour vous. Du
        moins, si vous ne dépendez pas des revenus publicitaires de Google, qui
        vous obligeront à céder à ces injonctions techniques. Et si vous évitez
        les outils annexes de Google, comme Maps au hasard, qui évoluent au bon
        vouloir de leur créateur. De plus, vous devrez parfois accepter
        <a href="https://twitter.com/johnath/status/1116871243301625856">quelques petits problèmes artificiels d’affichage et de compatibilité</a>.
        Et subir les malencontreuses performances dégradées des outils de
        Google sur les navigateurs concurrents. Et… La liste est sans fin.
      </p>

      <p>
        OK. Allons… Utilisons les
        <a href="https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea">technologies discutables développées à l’arrache par Google</a>,
        ce n’est pas si grave. Développons un site « optimisé pour Google
        Chrome ».
      </p>

      <p>
        Pas si grave pour vous. Mais pour les autres chargés d’implémenter des
        navigateurs et des outils web, l’enfer commence. Comment suivre le
        rythme effréné des nouveautés de Google, qui non content d’avoir une
        force de développement phénoménale, se file un sacré coup de pouce en
        choisissant les technologies que tout le monde devra implémenter ?
        Comment intégrer dans une base de code inadaptée des fonctionnalités
        qui s’encastrent comme par magie dans les outils de Google ?
      </p>

      <p>
        Par la force des choses, vous faites des autres navigateurs des outils
        de seconde zone.
      </p>

      <figure>
        <img src="{{ url_for('static', filename='images/00037-ie-mozilla.jpg') }}" alt="Gâteau envoyé par Mozilla à Microsoft pour la sortie d’IE10" title="Salut, copain ! Serrons-nous les coudes, parce que dans quelques années on aura totalement disparu." />
        <figcaption>
          Gâteau envoyé par Mozilla à Microsoft pour la sortie d’Internet
          Explorer 10. C’est sympa de s’envoyer des sucreries comme ça,
          pépères, mais en attendant j’en connais un qui va vous doubler tous
          les deux.
        </figcaption>
      </figure>
    </section>

    <section>
      <h3>Le piège de la situation</h3>

      <p>
        Oui. Google abuse de sa position dominante, volontairement ou non.
        Pourquoi sont-ils méchants ? Et d’ailleurs, sont-ils réellement
        méchants ? C’est discutable.
      </p>

      <p>
        Google est l’entreprise qui a participé à la déchéance d’Internet
        Explorer, quitte à
        <a href="https://blog.chriszacharias.com/a-conspiracy-to-kill-ie6">alimenter une conspiration visant à « tuer IE6 »</a>.
        Google a ouvertement critiqué les positions hégémoniques de Microsoft,
        lorsqu’il faisait partie des outsiders. Mais maintenant que
        l’entreprise est en position dominante, elle abuse des mêmes techniques
        vicieuses que celles qu’elle dénonçait.
      </p>

      <p>
        Et, comble de l’ironie,
        <a href="https://www.nextinpact.com/archive/50917-google-chrome-microsoft-monopole-europe.htm">Microsoft se doutait il y a plus de 10 ans que cela arriverait</a>.
      </p>

      <p>
        Le problème n’est pas tant dans les entreprises, qui seraient bonnes ou
        mauvaises. Elle est dans leur position de force, qui leur permet
        d’imposer de maintes manières leurs choix, et de conforter ainsi leur
        position de force, dans une boucle auto-alimentée. Cette toute
        puissance les mènera d’une manière ou d’une autre à leur chute, un jour
        où l’autre. Mais il serait appréciable de ne pas subir dans les
        ténèbres en attendant cette échéance.
      </p>

      <p>
        Comment faire…
      </p>

      <p>
        Patience ! Nous explorerons quelques lueurs d’espoir dans
        <a href="{{ url_for('blog', article='00038-bonjour-diversite-du-web') }}">le prochain article</a>…
      </p>
    </section>

  </article>

{% endblock content %}
