{% set title = 'Blog − En long, en large et sans travers' %}
{% set description = 'On ne met pas en forme un document de 1 000 pages comme on mettrait en page 1 000 documents d’une page. Et ce n’est pas une raison de faire 1 000 fois moins attention !' %}
{% set image = url_for('static', filename='images/00031-vignette.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>En long, en large et sans travers</h2>
      <aside><time datetime="2021-05-31">31 mai 2021</time>, par Stella et <a href="https://madcats.fr/">Madcats</a></aside>
      <p>
        On ne met pas en forme un document de 1 000 pages comme on mettrait en
        page 1 000 documents d’une page. Et ce n’est pas une raison de faire
        1 000 fois moins attention !
      </p>
    </header>

    {% include 'articles/_documents_contents.jinja2' %}

    <section>
      <h3>Un exemple de document</h3>

      <p>
        Trêve de discussions, voici un exemple de ce qu’il est possible de
        réaliser.
      </p>

      <figure>
        <a href="{{ url_for('static', filename='images/00031-all-cats-are-beautiful.pdf')}}#page=1">
          <img style="width: 30%" src="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-cover.png')}}" alt="Une du roman « All Cats Are Beautiful »" title="La belle couverture !" />
        </a>
        <a href="{{ url_for('static', filename='images/00031-all-cats-are-beautiful.pdf')}}#page=23">
          <img style="width: 30%" src="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-chapter-start.png')}}" alt="Tête de chapitre du roman « All Cats Are Beautiful »" title="La belle tête de chapitre !" />
        </a>
        <a href="{{ url_for('static', filename='images/00031-all-cats-are-beautiful.pdf')}}#page=22">
          <img style="width: 30%" src="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-chapter-stop.png')}}" alt="Fin de chapitre du roman « All Cats Are Beautiful »" title="La belle fin de chapitre !" />
        </a>
        <figcaption>
          Quelques pages d’un roman. C’est sacrément joli, et pourtant…
        </figcaption>
      </figure>

      <p>
        Ce roman n’est pas mis en page manuellement, la mise en page est
        réalisée avec un simple ensemble de règles. Et pourtant c’est joli, ça
        ne ressemble pas à ce qu’un ordinateur pourrait créer automatiquement !
      </p>
      <p>
        Suivre des règles n’empêche pas d’injecter une bonne dose de
        créativité. Ici, de nombreuses images ont par exemple permis de rendre
        le résultat original. La patte graphique est très reconnaissable, les
        éléments visuels sont attractifs, et tout laisse à croire que l’on est
        devant un ouvrage très particulier.
      </p>
      <p>
        La police de caractères utilisée est également singulière : c’est
        <a href="https://opendyslexic.org/">OpenDyslexic</a>, une police
        spécialement créée pour aider la lecture des personnes dyslexiques.
      </p>
      <p>
        Les images et le texte étant les principaux acteurs de ce livre, il
        n’est dès lors pas étonnant d’avoir l’impression d’être devant une
        création différente lorsque ces deux éléments se démarquent des romans
        dont on a l’habitude.
      </p>
      <p>
        Cependant, à bien y regarder, ce livre est comme les autres : une
        couverture, une table des matières, des chapitres… Comme nous l’avons
        vu dans
        <a href="{{ url_for('blog', article='00030-c-est-quoi-le-rapport') }}">l’article précédent</a>,
        rien ne ressemble plus à un livre qu’un autre livre, et c’est cet
        ensemble de règles qui rend le processus d’automatisation relativement
        facile, et même souhaitable.
      </p>
    </section>

    <section>
      <h3>C’est du classique</h3>

      <p>
        Si l’on extrait la substantifique moelle de ce livre, on obtient ce que
        l’on pourrait appeler un livre de poche classique.
      </p>

      <figure>
        <a href="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-classical.pdf')}}#page=1">
          <img style="width: 30%; border: 1px solid" src="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-classical-cover.png')}}" alt="Une classique du roman « All Cats Are Beautiful »" title="La belle couverture classique !" />
        </a>
        <a href="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-classical.pdf')}}#page=7">
          <img style="width: 30%; border: 1px solid" src="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-classical-chapter-start.png')}}" alt="Tête de chapitre classique du roman « All Cats Are Beautiful »" title="La belle tête de chapitre classique !" />
        </a>
        <a href="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-classical.pdf')}}#page=17">
          <img style="width: 30%; border: 1px solid" src="{{ url_for('static', filename='images/00031-all-cats-are-beautiful-classical-chapter-stop.png')}}" alt="Fin de chapitre classique du roman « All Cats Are Beautiful »" title="La belle fin de chapitre classique !" />
        </a>
        <figcaption>
          Les mêmes pages du même roman. C’est pareil, mais c’est pas pareil.
        </figcaption>
      </figure>

      <p>
        Avec une telle présentation, on retrouve pêle-mêle les grands
        classiques que nous avons vus auparavant. Parmi ceux-là, notons par
        exemple :
      </p>
      <ul>
        <li>une table des matières,</li>
        <li>des numéros de page,</li>
        <li>des titres de chapitres en en-têtes ou pieds de pages,</li>
        <li>une première et une quatrième de couverture,</li>
        <li>des chapitres sur pages de droite avec des titres,</li>
        <li>des paragraphes indentés…</li>
      </ul>
      <p>
        Lorsque l’on compare les deux documents, peu de différences viennent
        modifier les grandes règles de mise en page. Il est bien sûr possible
        de prendre quelques libertés pour des raisons graphiques, mais il est
        souvent rassurant et efficace de s’appuyer sur les règles solides.
      </p>
      <p>
        Il est important de prendre conscience d’une chose : le contenu est le
        même, les règles de mise en page sont sensiblement les mêmes. Le rendu
        est cependant très différent. Le premier regard sur le livre donne
        pourtant une véritable impression d’originalité, mais avec un œil
        attentif la matrice originale se dévoile à nouveau.
      </p>
      <p>
        La création graphique ne vient nullement remettre en cause la mise en
        page. Il est possible de produire deux livres visuellement très
        différents, parce que les illustrations sont différentes, sans changer
        une seule règle de mise en page. Toute la liberté peut se glisser dans
        les interstices, sans avoir à souffrir des canevas hautement conseillés
        pour des raisons d’habitude et de pratique.
      </p>
    </section>

    <section>
      <h3>Suivre les règles pour mieux s’en échapper</h3>

      <p>
        Voilà. Il n’est pas besoin d’opposer la technique et l’artistique. Il
        n’est pas besoin d’opposer la rigidité des règles et la liberté de la
        créativité. Il n’est pas besoin d’opposer le poids de l’histoire et le
        souffle de l’innovation.
      </p>
      <p>
        Les documents les plus longs sont aussi ceux qui permettent le mieux de
        marier un élégant classicisme avec une touche personnelle hors du
        commun. Pour l’un comme pour l’autre, la chose n’est pas aisée, mais
        elle s’apprend.
      </p>
      <p>
        Les outils de création de documents permettent d’accompagner toutes les
        parties prenantes. Bien maîtrisés, ils apportent une aide dans la
        définition et le respect des règles, comme dans la liberté artistique.
        Si vous avez toujours rêvé de gagner du temps sur les étapes
        répétitives, tout en bénéficiant d’une grande puissance créatrice, vous
        savez désormais que c’est possible. Il ne vous reste qu’à maîtriser les
        bons outils.
      </p>
    </section>
  </article>

{% endblock content %}
