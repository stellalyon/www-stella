{% set title = 'Blog − Le sens du détail' %}
{% set description = 'Même pour des factures, le moindre détail compte. Comment ajouter ce petit plus pour aller au-delà d’un document généré banal ? Avec un certain sens du détail…' %}
{% set image = url_for('static', filename='images/00027-facture-sober.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>Le sens du détail</h2>
      <aside><time datetime="2021-04-08">8 avril 2021</time>, par Stella et <a href="https://madcats.fr/">Madcats</a></aside>
      <p>
        Même pour des factures, le moindre détail compte. Comment ajouter ce
        petit plus pour aller au-delà d’un document généré banal ? Avec un
        certain sens du détail…
      </p>
    </header>

    {% include 'articles/_documents_contents.jinja2' %}

    <section>
      <h3>Le poids de l’histoire</h3>

      <p>
        Sans vouloir forcément atteindre le statut d’œuvre d’art (on va se
        calmer tout de suite),
        <strong>
          il est important de dépasser les questions de simple habillage pour
          aller dans le détail de la mise en page d’un document.
        </strong>
      </p>
      <p>
        C’est d’autant plus vrai pour les documents paginés. On écrit des
        livres depuis de nombreuses centaines d’années, avec les contraintes
        qui lui sont associées : pages aux dimensions fixes, rectos et versos,
        marges asymétriques… Sans le savoir, on a sous les yeux des conventions
        qui nous paraissent évidentes, mais que l’on a tendance à oublier
        lorsque l’on couche soi-même du contenu sur le papier.
      </p>
      <p>
        Nous ne nous plongerons pas dans une revue exhaustive de toutes ces
        règles ancestrales (ne me tentez pas), mais nous pouvons cependant voir
        deux points qui rendraient bien sur nos belles factures :
        l’alignement de chiffres et la coupure de tableaux.
      </p>
    </section>

    <section>
      <h3>Deux exemples</h3>

      <h4>Des chiffres et pas des lettres</h4>

      <p>
        Une facture − ne nous cachons pas derrière notre petit doigt − est
        avant tout une affaire de chiffres. Et comme vous le savez, les
        chiffres ne mentent pas… Alors, faites-leur dire à vos clients combien
        vous aimez le travail de qualité.
      </p>
      <p>
        (Oui, j’aime bien utiliser des aphorismes totalement hors de propos
        pour appuyer mes arguments.)
      </p>
      <p>
        Voilà d’où l’on part.
      </p>
      <figure>
        <a href="{{ url_for('static', filename='images/00028-chiffres-moches.png')}}">
          <img src="{{ url_for('static', filename='images/00028-chiffres-moches.png')}}" alt="Une facture avec des chiffres un peu moches" title="Des chiffres un peu moches" />
        </a>
        <figcaption>
          Vous avez déjà vu des factures comme ça. Vous avez peut-être même
          déjà fait des factures comme ça. Allez, avouez, on est entre nous.
        </figcaption>
      </figure>
      <p>
        L’alignement des chiffres est une première chose, et la bonne pratique
        est simple : pour les prix sur plusieurs lignes, alignez à droite. Pour
        peu que vous mettiez bien toujours deux chiffres après la virgule (et
        vous le faites, n’est-ce-pas ?), vous aurez déjà les virgules alignées
        les unes aux autres, et donc une plus grande facilité à ajouter ou
        comparer les montants. Parce que vérifier les chiffres sur les
        factures, c’est votre grande passion.
      </p>
      <figure>
        <a href="{{ url_for('static', filename='images/00028-chiffres-mieux.png')}}">
          <img src="{{ url_for('static', filename='images/00028-chiffres-mieux.png')}}" alt="Une facture avec des chiffres un peu mieux" title="Des chiffres un peu mieux" />
        </a>
        <figcaption>
          On a vu pire. Mais on a vu mieux.
        </figcaption>
      </figure>
      <p>
        Alignés, vraiment ? Pas exactement, parce que les chiffres ont la
        triste habitude de ne pas faire tous la même largeur. Vous avez vu que
        le « 11,11 » n’est pas aligné avec ses copains ? Le coupable est
        souvent le « 1 » qui s’amuse à être plus fin que ces camarades, preuve
        que les premiers sont les derniers (encore un aphorisme douteux).
      </p>
      <p>
        Une solution ? Bien sûr ! Les typographes, ces héros et héroïnes des
        temps anciens et modernes, ont pensé à nous : ils ont inclus dans les
        fontes sérieuses des
        <a href="http://designwithfontforge.com/fr-FR/Numerals.html#chiffre-de-style-align%C3%A9">chiffres tabulaires</a>
        qui s’alignent comme par magie. Après avoir activé
        l’option-qui-va-bien™, le résultat est encore meilleur.
      </p>
      <figure>
        <a href="{{ url_for('static', filename='images/00028-chiffres-tres-bien.png')}}">
          <img src="{{ url_for('static', filename='images/00028-chiffres-tres-bien.png')}}" alt="Une facture avec des chiffres très bien" title="Des chiffres très bien" />
        </a>
        <figcaption>
          Là, c’est chouette.
        </figcaption>
      </figure>
      <p>
        On s’arrête là ? Il y a toujours moyen de faire mieux, mais on est déjà
        montés en gamme. Et surtout, on a un autre sujet devant nous.
      </p>

      <h4>La table plaide non-coupable</h4>

      <p>
        Les tableaux sur les factures sont parfois l’objet de mises en page
        cocasses. Et quand je dis « cocasses », je veux en fait dire autre
        chose qui rime avec « cocasses », mais avec un peu moins de classe.
      </p>
      <figure>
        <a href="{{ url_for('static', filename='images/00028-ovh.png')}}">
          <img src="{{ url_for('static', filename='images/00028-ovh.png')}}" alt="Une facture d’OVH avec un tableau coupé n’importe comment" title="La catablastrophe" />
        </a>
        <figcaption>
          J’ai pris OVH, pas de bol pour eux, mais j’aurais en pu prendre
          beaucoup d’autres.
        </figcaption>
      </figure>
      <p>
        Les tableaux coupés n’importe comment sont monnaie courante. Et pour
        cause : couper un tableau correctement est déjà un véritable casse-tête
        pour un être humain, alors pour un ordinateur…
      </p>
      <p>
        Dans une facture, c’est encore plus compliqué, parce qu’on a
        généralement des tableaux qui ne jouent pas le même rôle : certains
        présentent les produits vendus, d’autres récapitulent les sommes
        totales. Couper la facture au milieu de ces tableaux, ou entre ces
        tableaux, n’est pas vraiment souhaitable. Enfin, ça dépend où…
      </p>
      <figure>
        <a href="{{ url_for('static', filename='images/00028-mauvais-exemple-1.png')}}">
          <img style="width: 30%" src="{{ url_for('static', filename='images/00028-mauvais-exemple-1.png')}}" alt="Une facture avec une mauvaise coupure de tableaux" title="Une mauvaise coupure" />
        </a>
        <a href="{{ url_for('static', filename='images/00028-mauvais-exemple-2.png')}}">
          <img style="width: 30%" src="{{ url_for('static', filename='images/00028-mauvais-exemple-2.png')}}" alt="Une (autre) facture avec une mauvaise coupure de tableaux" title="Une mauvaise coupure (seconde édition)" />
        </a>
        <a href="{{ url_for('static', filename='images/00028-mauvais-exemple-3.png')}}">
          <img style="width: 30%" src="{{ url_for('static', filename='images/00028-mauvais-exemple-3.png')}}" alt="Une facture (encore) avec une mauvaise coupure de tableaux" title="Une mauvaise coupure (on ne s’arrête plus)" />
        </a>
        <figcaption>
          Quelques exemples pas terribles qu’on préférerait éviter sur nos
          factures.
        </figcaption>
      </figure>
      <p>
        Là encore, nous n’allons pas nous lancer dans une liste exhaustive.
        Mais en réfléchissant un peu (beaucoup), on peut au moins lister
        quelques règles pour garder une présentation à peu près saine.
      </p>
      <ul>
        <li>Pas de coupure de page au milieu d’une ligne.</li>
        <li>Pas de coupure de page entre les titres de colonnes et les lignes</li>
        <li>Pas de coupure de page juste avant le tableau des totaux.</li>
        <li>Des titres de colonnes qui se répètent sur toutes les pages.</li>
      </ul>
      <p>
        Alors, on obtient quoi ?
      </p>
      <figure>
        <a href="{{ url_for('static', filename='images/00028-bon-exemple.png')}}">
          <img src="{{ url_for('static', filename='images/00028-bon-exemple.png')}}" alt="Facture avec une superbe coupure de tableau" title="Une superbe coupure" />
        </a>
        <figcaption>
          Ça a l’air super normal, mais on a dû faire chauffer nos neurones
          pour en arriver là ! C’est la dure loi de la mise en page : invisible
          quand elle est belle, insupportable quand elle n’est pas parfaite.
        </figcaption>
      </figure>

    </section>

    <section>
      <h3>Ce n’est qu’un début</h3>

      <p>
        Nous avons mis dans cet article le projecteur sur deux détails
        particulièrement sensibles des factures. Comme toutes les règles,
        celles qui sont présentées ici sont faites pour être parfois bafouées
        (la fameuse exception qui confirme la règle, c’était le dernier
        aphorisme du jour).</p>
      <p>
        Cela étant dit, dans le doute, n’hésitez cependant pas à les suivre
        sans prendre trop de risques. Car, comme vous le savez bien, prudence
        est mère de sûreté (oups, désolé, j’ai déjà récidivé).
      </p>
      <p>
        Et bien sûr, tout est parfois différent pour d’autres types de
        documents. La route est longue.
      </p>
      <p>
        Parce qu’il n’y a pas que les factures dans la vie (et c’est une bonne
        nouvelle, non ?), nous continuerons donc notre périple avec d’autres
        exemples, d’autres formats, d’autres problématiques.
      </p>
    </section>
  </article>

{% endblock content %}
