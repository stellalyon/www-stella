{% set title = 'Blog − L’enfer des paquets Python : les racines du mal' %}
{% set description = 'La gestion des paquets Python est parfois un enfer. La faute à la
        stupidité des personnes derrière Python ? Et si c’était un peu plus
        compliqué que cela ?' %}
{% set image = url_for('static', filename='images/00005-image.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>L’enfer des paquets Python : les racines du mal (2 / 7)</h2>
      <aside><time datetime="2020-06-16">16 juin 2020</time>, par Guillaume</aside>
      <p>
        La gestion des paquets Python est parfois un enfer. La faute à la
        stupidité des personnes derrière Python ? Et si c’était un peu plus
        compliqué que cela ?
      </p>
    </header>

    {% include 'articles/_python_contents.jinja2' %}

    <section>
      <h3>Ce n’est pas la bonne techno</h3>
      <p>
        Nous avons vu
        <a href="{{ url_for('blog', article='00003-l-enfer-des-paquets-python-le-sac-de-noeuds') }}">la dernière fois</a>
        qu’il était parfaitement possible de faire les choses bien concernant
        la création et la distribution de paquets, qu’il n’y avait qu’à
        regarder ce que faisaient par exemple les gens de Rust, et que ce
        n’était pas somme toute si compliqué.
      </p>
      <p>
        Ce n’est pas totalement faux, mais soyons honnêtes, ce n’est pas
        totalement vrai non plus. Tout d’abord, Rust bénéficie d’atouts
        techniques indéniables par rapport à Python, avec par exemple la
        possibilité de distribuer des bibliothèques et des exécutables
        compilés. Python étant interprété, se pose la question de la
        distribution et de l’installation de l’interpréteur, avec tout son lot
        de questions annexes (quel interpréteur, quelle version…). Certains
        outils comme <a href="http://nuitka.net/">Nuitka</a> visent à apporter
        des embryons de solutions dans ce sens, mais ils sont condamnés à vivre
        leur existence en dehors de la bibliothèque standard, et donc
        probablement d’une utilisation massive.
      </p>
      <p>
        Au-delà de l’aspect technique, le principal avantage de Rust est son
        âge. Créé en 2010, il est un jeune enfant comparé à l’antique Python né
        environ 20 ans plus tôt. Entre 1990 et 2010 sont apparus CVS,
        Subversion et Git ; Netscape, Internet Explorer, Firefox et Chrome ;
        HTML, CSS et JavaScript. C’est incroyable, mais c’est vrai, et ça met
        un peu en perspective la situation relative de Python et Rust.
      </p>
    </section>

    <section>
      <h3>Ce n’est pas le bon moment</h3>
      <p>
        On a du mal à se souvenir ou imaginer comment était l’informatique
        quand Python a commencé à germer à la fin des années 80, mais on arrive
        sans grande peine à comprendre pourquoi la création et la distribution
        de paquets n’étaient pas à la pointe des problèmes à prendre en compte.
      </p>
      <figure>
        <img src="{{ url_for('static', filename='images/00005-netscape.png')}}" alt="Écran de démarrage de Netscape 6" title="Et c’est parti pour 20 secondes de démarrage…" />
        <figcaption>
          Le bel écran de démarrage de Netscape 6 sorti en 2000, la même année
          que <code>distutils</code>.
        </figcaption>
      </figure>
      <p>
        Python n’a bien sûr pas intégré d’outils pour distribuer le code dès
        ses débuts. La Python Package Authority (PyPA) maintient
        <a href="https://www.pypa.io/en/latest/history/">un historique</a> très
        instructif de l’évolution des paquets, où l’on apprend entre autres que :
      </p>
      <ul>
        <li>
          distutils a été intégré dans la bibliothèque standard en 2000, pour
          Python 1.6 ;
        </li>
        <li>
          PyPI a été mis en ligne en 2003 ;
        </li>
        <li>
          la PyPA a été créée en 2011 pour s’occuper de <code>pip</code> (né en
          2008) et <code>virtualenv</code> (né en 2007) ;
        </li>
        <li>
          Python 3.3 a failli intégrer le remplaçant de <code>distutils</code> et
          <code>setuptools</code>, mais le projet a été abandonné faute
          d’investissement ;
        </li>
        <li>
          un nombre indécent de PEP ont été proposées et acceptées concernant
          l’évolution de ces outils.
        </li>
      </ul>
      <p>
        Le plus frappant est peut-être l’impression d’amateurisme, en
        particulier à la naissance des premiers outils. Quiconque a déjà
        utilisé <code>easy_install</code> sait que la situation était
        alors extrêmement douloureuse, que l’installation d’un paquet
        nécessitait une dose non négligeable de persévérance, de connaissances
        techniques, et bien sûr de chance. L’accumulation de noms, d’outils, de
        bibliothèques internes et externes montre que tout le monde est parti
        bille en tête dans des solutions partielles, hasardeuses, voire
        carrément bancales.
      </p>
      <p>
        Comme on peut l’entendre dans
        <a href="https://www.pythonpodcast.com/pip-resolver-dependency-management-episode-264/">cet excellent épisode de Podcast.__init__</a>,
        l’écriture des outils a longtemps été faite avant de soumettre à
        discussion et acceptation une spécification formelle. Par manque de
        temps, et par manque de moyens également : il n’y a pas d’entreprise
        derrière PyPI ou PyPA, comme il y en a par exemple derrière
        <a href="https://en.wikipedia.org/wiki/Npm_(software)#The_company"><code>npm</code></a>.
        La grande majorité des développements sont faits par des bénévoles, qui
        ne seraient pas contre un coup de main ✋ (oui, c’est un appel du pied 🦶).
      </p>
      <p>
        Certes, cet état des lieux n’explique pas tout. D’autres changements
        majeurs ont été intégrés dans le langage avec beaucoup plus de tact, en
        particulier récemment
        <a href="https://www.python.org/dev/peps/pep-0492/">les coroutines</a>.
        Cette fonctionnalité a nécessité une discussion large et houleuse avant
        une intégration plutôt appréciée. Avant de graver ces changements de
        syntaxe dans le marbre du langage,
        <a href="https://docs.python.org/3/library/asyncio-task.html?highlight=coroutine#generator-based-coroutines">des propositions moins intrusives</a>
        ont été testées et éprouvées, rendant la solution finalement retenue
        plus largement acceptable.
      </p>
      <p>
        Il est facile de jeter ses pierres acérées sur les gens qui ont créé
        ces outils. Il est facile de vilement railler le manque de vision de
        l’équipe derrière Python, qui a intégré à la va-vite certaines
        solutions discutables et qui en a laissé d’autres aux portes de la
        bibliothèque standard. Mais si l’on pense que <code>distutils</code> a
        été intégré à un moment où le navigateur en vogue s’appelait Internet
        Explorer 5.5…
      </p>
      <p>
        Et en plus, il n’est pas trop tard pour changer tout cela. Si ?
      </p>
    </section>

    <section>
      <h3>Ce n’est pas le bon standard</h3>
      <p>
        En quelque sorte, malheureusement, si, c’est trop tard.
      </p>
      <figure>
        <img src="{{ url_for('static', filename='images/00005-xkcd.png') }}" alt="Mandatory Related XKCD™" title="Fortunately, the charging one has been solved now that we've all standardized on mini-USB. Or is it micro-USB? Shit." />
        <figcaption>
          Le <a href="https://xkcd.com/927/">XKCD</a> obligatoire
        </figcaption>
      </figure>
      <p>
        Le problème est qu’il existe depuis le début des années 2000 des
        paquets dans la nature, sur PyPI, dans des dépôts publics et
        privés. Ces paquets sont utilisés par de nombreuses personnes, parfois
        sur des systèmes obsolètes (je te regarde, Python 2). Et Python, qui a
        déjà douloureusement éprouvé les changements brutaux et incompatibles
        (je te regarde, Python 3) ne voudra certainement pas rendre
        ininstallable cette masse informe et nébuleuse de paquets.
      </p>
      <p>
        Sinon, ça va sortir les fourches et les guillotines.
      </p>
      <p>
        Toutes les propositions, que ce soit dans les fichiers
        (<code>requirements.txt</code>, <code>setup.py</code>,
        <code>Pipfile</code>, <code>setup.cfg</code>,
        <code>pyproject.toml</code>…) ou dans les outils
        (<code>easy_install</code>, <code>pip</code>, <code>pipenv</code>,
        <code>poetry</code>, <code>setuptools</code>, <code>distutils</code>…)
        ne peuvent que s’ajouter les unes aux autres, sans jamais parfaitement
        les remplacer, sans jamais jeter aux oubliettes les défauts de leurs
        illustres prédécesseurs.
      </p>
      <p>
        Alors évidemment, tout n’est pas perdu. Les changements les plus
        importants vont aujourd’hui globalement dans le sens d’une
        spécification, d’une rationalisation et d’une simplification
        systématiques. De nombreuses PEP sont proposées pour décrire et
        discuter avant de coder, pour que les détails d’implémentation ne
        fassent plus la loi au détriment d’idées nées de consensus.
      </p>
      <p>
        Mais le chemin est long.
      </p>
    </section>

    <section>
      <h3>Ce n’est pas la bonne solution</h3>
      <p>
        Si vous pensez que la gestion de dépendances est un problème résolu de
        longue date et que les personnes derrière <code>pip</code> manquent
        sérieusement de compétences, rappelez-vous que le nom d’un paquet
        Python est souvent donné dans le fichier <code>setup.py</code>, et
        qu’il peut donc en théorie dépendre d’informations comme le système
        d’exploitation, la présence de modules externes ou même de l’heure
        qu’il est. Connaître les modules dont dépend un paquet nécessite
        parfois de lancer un interpréteur, et l’arbre de dépendances est donc
        différent pour chaque personne qui installe un paquet.
      </p>
      <p>
        En pratique, des solutions ont été trouvées pour pallier cette
        flexibilité qui vire clairement au cauchemar dans certains cas. Il
        n’est bien heureusement pas nécessaire de télécharger et d’exécuter
        toutes les versions possibles de tous les paquets avant de déterminer
        celles qu’il faut installer. Mais tout cela se fait avec de nouveaux
        formats et de nouvelles métadonnées qui, par définition, ne font pas
        partie des paquets précédents.
      </p>
      <p>
        Concrètement, cela signifie que pour les outils d’installation des
        paquets, la gestion des solutions obsolètes va encore durer de
        nombreuses et douloureuses années. Le rythme effréné des nouvelles
        versions de
        <a href="https://pypi.org/project/setuptools/#history"><code>setuptools</code></a>
        et
        <a href="https://pypi.org/project/pip/#history"><code>pip</code></a>
        donnent une idée des améliorations constantes qui se trament sans que
        nous nous en rendions compte ; mais nous ne sommes pas prêts de sortir
        de l’historique démoniaque que nous devrons trimballer longtemps
        encore.
      </p>
      <p>
        Cependant, pour les personnes qui créent des paquets, la solution
        s’améliore considérablement. Le sujet est relativement peu traité et
        il est compliqué de trouver une documentation de référence sur le sujet
        (même de la part de PyPA, surtout de la part de PyPA). C’est bien
        dommage, parce qu’il deviendrait presque facile et élégant ces derniers
        mois de faire des paquets Python.
      </p>
      <p>
        Sans <code>setup.py</code> du tout, par exemple.
      </p>
      <p>
        Vous aimeriez en savoir plus ? Il nous reste cinq articles pour faire
        un tour de l’outillage existant, mieux définir ce que nous voulons
        faire, avant de finalement créer notre paquet de toute beauté.
      </p>
      <p>
        <a href="{{ url_for('blog', article='00007-l-enfer-des-paquets-python-la-folie-des-formats') }}">À suivre…</a>
      </p>
    </section>
  </article>

{% endblock content %}
