{% set title = 'Blog − Derrière la pseudonymisation' %}
{% set description = 'Précédemment, nous avons vu la différence entre l’anonymisation et la
      pseudonymisation. Et si nous regardions plus en détail ce qui se cache
      derrière la pseudonymisation ?' %}
{% set image = url_for('static', filename='images/00008-image.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>Derrière la pseudonymisation</h2>
      <aside><time datetime="2020-07-09">9 juillet 2020</time>, par Lucie</aside>
      <p>
      <a href="https://www.stella.coop/blog/00006-pseudonymisation-vs-anonymisation.html">
        Précédemment</a>, nous avons vu la différence entre l’anonymisation et la
      pseudonymisation. Et si nous regardions plus en détail ce qui se cache
      derrière la pseudonymisation ?
      </p>
    </header>

    <section>
      <h3>Dans quels scénarios ?</h3>
      <p>
      La pseudonymisation est amenée à être un élément important dans le cadre
      du RGPD en tant que mesure de sécurité. En effet, elle permet de protéger,
      dans une certaine mesure, l’identité des personnes. Elle permet également
      de minimiser le nombre de données personnelles traitées quand il n’est pas
      nécessaire de connaître la vraie identité des utilisateurs pour un
      traitement particulier.
      </p>

      <p>
      La pseudonymisation peut être utilisée dans différents scénarios dépendants
      des buts des organismes prenant part aux traitements comme :
      </p>

      <ul>
        <li>pour un usage interne à une entité : améliorer la sécurité
          des données lors du partage des informations entre différents services
          et en cas d’incident relatif à la sécurité de manière générale ;</li>
        <li>pour le partage avec une entité tierce : les données sont collectées
          par un organisme qui fait appel à un autre organisme, par exemple pour
          des analyses statistiques sur l’usage d’un service ;</li>
        <li>dans l’exploitation de données depuis une entité tierce : les données
          sont collectées et pseudonymisées par un autre organisme qui va ensuite
          les partager avec l’organisme principal, par exemple pour de la mesure
          d’audience.</li>
      </ul>
    </section>

    <section>
      <h3>Quelles techniques ?</h3>
      <p>
      Pour pseudonymiser une donnée, il suffit de lui appliquer une fonction qui
      la transforme. Pour deux données distinctes, la fonction de pseudonymisation
      renvoie deux pseudonymes distincts. Pour cela, différentes méthodes existent
      (liste non exhaustive) :
      </p>

      <dl>
        <dt>Le compteur</dt>
        <dd>
        La donnée à pseudonymiser est simplement associée à sa position dans la
        liste des données à traiter. La première donnée est associée à l’identifiant
        1, la deuxième à l’identifiant 2, la troisième à l’identifiant 3, etc.
        </dd>

        <dt>Le générateur de nombre aléatoire</dt>
        <dd>
        Comme pour le compteur, une donnée est associée à un nombre. Mais ce coup-ci,
        le nombre est généré aléatoirement ; ce qui présente l’intérêt de ne pas
        dévoiler l’ordre des données.
        </dd>

        <dt>La fonction de hachage cryptographique</dt>
        <dd>
        À partir de données de différentes tailles, cette fonction renvoie de
        nouvelles données de même taille. L’avantage de cette méthode est qu’elle
        permet de s’assurer, par définition, qu’une donnée a un pseudonyme unique.
        Quelques
        <a href="https://en.wikipedia.org/wiki/Cryptographic_hash_function#Cryptographic_hash_algorithms">exemples</a>
        de fonction de hachage : MD5, SHA-1, SHA-2…
        </dd>

        <dt>Le code d’authentification de message (MAC)</dt>
        <dd>
        Cette méthode est similaire à la précédente. La différence est que pour
        transformer une donnée, une clé secrète est utilisée en plus de la fonction
        de hachage ; ce qui rend la méthode plus robuste, tant que la clé secrète
        est toujours secrète.
        </dd>
      </dl>

      <h4>Un petit exemple</h4>
      <p>
      Voici un exemple avec des adresses mail des différentes méthodes de
      pseudonymisation. Pour le hachage et le code d’authentification de message,
      l’algorithme MD5 a été utilisé.
      </p>

      <table>
        <thead>
          <tr>
            <td>Adresses mail</td>
            <td>Compteur</td>
            <td>Nombre aléatoire</td>
            <td>Hachage</td>
            <td>MAC</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>francois@dupont.fr</td>
            <td>1</td>
            <td>12</td>
            <td>466123cf6be1eef6cbf70495bb87cd24</td>
            <td>bd9de45a38e970a17ebf300866c8bbae</td>
          </tr>
          <tr>
            <td>francine@michu.fr</td>
            <td>2</td>
            <td>51</td>
            <td>86a313d58ef1c26b5dae16b691d0aa62</td>
            <td>da93151bc9353cdbf5b7d613d5cc4460</td>
          </tr>
          <tr>
            <td>joseph@clinton.fr</td>
            <td>3</td>
            <td>42</td>
            <td>0d2d86c8fd843d177384606200cc25b2</td>
            <td>cca373aa92a38c0f47192b420d0c231e</td>
          </tr>
          <tr>
            <td>julie@fernand.fr</td>
            <td>4</td>
            <td>66</td>
            <td>c9bb585d0d189b27bd8dfbe210fcb604</td>
            <td>c5e6177da4b8e8e8369912dd31e2d87c</td>
          </tr>
        </tbody>
      </table>
      <p>
      L’adresse a été traitée en un seul bloc, mais on peut appliquer chaque
      méthode sur une partie de l’adresse pour conserver l’information sur la
      nature de la donnée. Ce qui donnerait quelque chose comme
      <code>12@666</code> pour la méthode du nombre aléatoire.
      </p>
    </section>

    <section>
      <h3>Les limites de la pseudonymisation</h3>
      <p>
      Des personnes ou des entités peuvent avoir un intérêt à casser la
      pseudonymisation d’un ensemble de données ; que ce soit pour trouver la
      méthode de pseudonymisation utilisée, ré-identifier les données ou isoler
      une information précise de l’ensemble.
      </p>
      <p>
      Pour cela, différentes techniques peuvent être mises en œuvre, suivant
      l’objectif visé, les connaissances sur l’ensemble de données, la taille
      de l’ensemble, etc. On peut par exemple utiliser l’attaque par
      <a href="https://fr.wikipedia.org/wiki/Attaque_par_force_brute">force brute</a>,
      la recherche <a href="https://fr.wikipedia.org/wiki/Attaque_par_dictionnaire">par dictionnaire</a>
      ou encore par conjectures.
      </p>

      <p>
      Même si la pseudonymisation est cassable, sa plus grande limite est qu’il
      est facile de lier des données entre elles, même lorsqu’elles sont
      pseudonymisées et d’en tirer des informations.
      </p>

      <h4>Un exemple</h4>
      <p>
      Une sociéte met en ligne un service sur lequel les personnes peuvent
      s’inscrire. Cette société fait appel à un prestataire pour gérer la sécurité
      de son service et de ses accès.
      </p>
      <p>
      Pour permetter au prestataire d’accomplir sa mission, la société lui
      transmet les fichiers contenant les adresses IP pseudonymisées et les heures
      d’accès de ces IPs sur le service.
      </p>
      <p>
      Même si le prestataire n’arrive pas à casser la pseudonymisation des adresses IP,
      il peut avoir d’assez bonnes estimations sur l’utilisation du service avec
      les métriques suivantes par exemple :
      </p>
      <ul>
        <li>le nombre d’utilisateurs uniques ;</li>
        <li>le nombre de nouveaux inscrits ;</li>
        <li>le nombre d’utilisateurs qui reviennent sur le service.</li>
      </ul>
      <p>
      Ces métriques reposant sur les adresses IP, il peut y avoir des imprecisions.
      Mais les IPs changeant peu, les estimations sont valables.
      </p>
      <p>
      Le prestataire n’a pas d’intérêt à connaître la véritable identité des
      utilisateurs, mais les informations qu’il est capable d’extraire des données,
      même pseudonymisées, lui donnent un avantage au niveau des accords et négociations
      avec la société cliente.
      </p>

      <p>
      La pseudonymisation reste donc une mesure de sécurité utile lorsque l’on traite
      des données personnelles. Mais elle possède des limites car elle
      laisse transparaître beaucoup d’informations sur les données, même si la méthode
      utilisée pour les pseudonymiser n’est pas partagée.
      La pseudonymisation toute seule ne constitue pas une réelle sécurité mais
      fait partie d’un ensemble de mesures ayant pour but un certain niveau de sécurité.
      </p>
      <p>
      Pour aller plus loin sur le sujet, un guide sur les techniques de pseudonymisation
      réalisé par l’ENISA est <a href="https://www.enisa.europa.eu/publications/pseudonymisation-techniques-and-best-practices">
        disponible</a>.
    </section>
  </article>

{% endblock content %}
