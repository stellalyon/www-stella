{% set title = 'Blog − Bonjour, diversité du web' %}
{% set description = 'Vous lisez cet article en utilisant Google Chrome ? C’est fort probable, comme presque tout le monde, mais… Le lisez-vous sur le grand écran d’un bon vieil ordinateur, aux toilettes sur votre téléphone, dans le lit sur votre console ? Ou, même, l’écoutez-vous à l’aide d’une synthèse vocale ? Chrome est partout, certes, mais la diversité des usages pourrait bien sauver la diversité des navigateurs.' %}
{% set image = url_for('static', filename='images/00038-heartbleed.svg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>Bonjour, diversité du web</h2>
      <aside><time datetime="2021-12-07">7 décembre 2021</time>, par Guillaume</aside>
      <p>
        Vous lisez cet article en utilisant Google Chrome ?
        C’est fort probable, comme presque tout le monde, mais…
        Le lisez-vous sur le grand écran d’un bon vieil ordinateur, aux
        toilettes sur votre téléphone, dans le lit sur votre console ? Ou,
        même, l’écoutez-vous à l’aide d’une synthèse vocale ? Chrome est
        partout, certes, mais la diversité des usages pourrait bien sauver la
        diversité des navigateurs.
      </p>
    </header>

    <section>
      <h3>Web partout, justice nulle part</h3>

      <h4>La tête qui dépasse</h4>

      <p>
        Le web est partout : chez vous dans toutes vos pièces, dans le métro,
        dans les avions, sur les mers, dans l’espace. Le web est partout : au
        travail pour travailler, à la maison pour s’amuser, ou l’inverse. Le
        web est partout : le matin au réveil pour écouter les dernières
        nouvelles, à midi pour jouer pendant la pause, le soir pour regarder
        une série, et la nuit pour s’endormir en musique.
      </p>

      <p>
        Beaucoup de personnes ont travaillé dur pour que le web soit partout.
        Si on ne parle que de la couche logicielle (c’est injuste pour les
        trésors d’inventivité du matériel, mais que voulez-vous, il nous
        faudrait une vie entière pour parler en détail de tous les sujets), on
        pensera par exemple aux personnes derrière les protocoles du Wi-Fi, les
        algorithmes de chiffrage, les spécifications de CSS, des parseurs de
        HTML.
      </p>

      <p>
        Ah, bah non. En fait, non, pas du tout. On n’y pense jamais. Vraiment
        jamais.
      </p>

      <p>
        On clique sur l’icône de Chrome, et furtivement, on pense à Google.
        Qu’on le veuille ou non.
      </p>

      <p>
        Si son moteur de recherche a révolutionné le monde, Google a pourtant
        été à la traîne concernant son navigateur. Chrome est né en 2008, soit
        5 ans après Safari, 6 ans après Firefox, 13 ans après Opera et Internet
        Explorer. Il a aujourd’hui tout raflé, laissant les miettes à ses
        adversaires que les plus jeunes d’entre nous ne connaissent peut-être
        même pas tous.
      </p>

      <h4>La « théorie » du ruissellement</h4>

      <p>
        Il est peut-être certains domaines où le ruissellement fonctionne, où
        une tête de proue ferait redescendre, de proche en proche, la richesse
        et la reconnaissance à l’ensemble des parties prenantes. Pour les
        outils du web, ça ne fonctionne pas vraiment.
      </p>

      <p>
        Pourtant, dans le domaine, Google ferait presque figure de bon élève :
        l’entreprise met en avant son implication technique et financière pour
        développer de nombreux logiciels libres, en particulier avec son
        <a href="https://fr.wikipedia.org/wiki/Google_Summer_of_Code">Summer of Code</a>.
        De <a href="https://github.com/google/">nombreux outils nés chez eux sont libres</a>,
        et on pourrait difficilement les accuser de ne pas contribuer au monde
        du logiciel libre.
      </p>

      <p>
        Pourtant, ce n’est pas suffisant. Certains exemples montrent la cruelle
        situation dans laquelle sont certains projets libres pourtant utilisés,
        sans le savoir par une immense majorité des simples utilisateurs du
        web. Prenons-en deux.
      </p>

      <figure>
        <img src="{{ url_for('static', filename='images/00038-heartbleed.svg') }}" alt="Logo de la faille de sécurité Heartbleed" title="Ne laissez pas saigner le cœur du web…" />
        <figcaption>
          La faille de sécurité Heartbleed a cruellement mis en évidence le
          manque de moyens déployés pour maintenir OpenSSL, malgré des
          milliards d’utilisateurs quotidiens.
        </figcaption>
      </figure>

      <p>
        Le premier est OpenSSL. Bibliothèque mettant à disposition des outils
        de chiffrement utilisée massivement, entre
        autres, par les clients et les serveurs HTTP, c’est une pierre
        angulaire sans laquelle il serait compliqué de faire fonctionner le
        web. Pourtant, quand en 2014 une faille de sécurité appelée Heartbleed
        a été rendue publique, tous les regards se sont soudain braqués sur
        l’équipe responsable de cet échec. « Équipe » est un grand mot, quand
        on sait qu’une seule personne travaillait à temps plein sur le
        logiciel. Et
        <a href="https://web.archive.org/web/20161202163921/http://blog.ssh.com/free-can-make-you-bleed">le monde a découvert la précarité d’OpenSSL</a>,
        perdu parmi tant d’autres briques ignorées bien qu’essentielles.
      </p>

      <p>
        Le second est cURL, un outil permettant de récupérer le contenu d’une
        ressource sur un réseau. Tout comme OpenSSL, cURL est installé partout,
        sur environ 10 milliards de terminaux informatiques. Impressionnant,
        n’est-ce pas ? Sur quelle « équipe » repose ce formidable outil ? À
        vrai dire, 95 % du travail a été accompli par une seule personne. Une
        seule personne, c’est tout ? En fait, c’est même un peu moins. Il a
        fallu
        <a href="https://daniel.haxx.se/blog/2021/01/15/food-on-the-table-while-giving-away-code/">plus de 20 ans de développement</a>
        avant que cette personne ne soit payée pour travailler à plein temps
        sur cURL.
      </p>

      <p>
        Quel rapport avec notre sujet, me direz-vous ? Pourquoi prendre ces
        deux exemples, à part pour se plaindre, une fois de plus, de
        l’injustice du monde dans lequel on vit ?
      </p>

      <p>
        Parce que c’est une source d’espoir.
      </p>
    </section>

    <section>
      <h3>Nous sommes légion</h3>

      <p>
        Le web n’est pas seulement ce que nous laissent voir les navigateurs.
        Derrière la grande magie du site internet se déploient des dizaines,
        des centaines d’outils de l’ombre qui ont en charge de faire transiter
        l’information à travers le monde. Ce sont les rouages de l’horloge, les
        lutins du Père Noël, les molécules du nuage : toujours là, mais
        rarement visibles.
      </p>

      <p>
        Tous ces outils parlent les mêmes protocoles, les mêmes formats, les
        mêmes normes. Ils discutent entre eux, automatiquement, sans que l’on
        ait besoin d’autre chose que des langages communs et des procédures
        interopérables.
      </p>

      <p>
        Cette magie n’en est pas une. Il a fallu tout le génie et le travail
        des architectes derrière ces protocoles, formats, normes. Avec une
        incroyable stabilité, ces technologies ont vu naître et mourir des
        utilisations incroyablement variées, d’une simple page de texte à des
        jeux 3D matériellement accélérés. Les bases sont restées assez stables
        pour qu’<a href="http://info.cern.ch/hypertext/WWW/TheProject.html">un site créé il y a plus de 30 ans</a>
        s’affiche sans aucun problème dans votre navigateur actuel. Non, ne
        c’est pas de la magie, ce n’est pas le fruit du hasard. Des gens ont
        vraiment réfléchi très longtemps à cela. Le fait que cela fonctionne
        n’est pas normal, c’est une véritable prouesse technique.
      </p>

      <p>
        C’est la diversité qui, entre autres, donne au web cette sorte de
        stabilité. Parce qu’il est impossible de faire changer d’un seul coup
        tous les serveurs, les clients, les pages, les outils utilisés sur le
        web, il faut bien que chaque partie prenante tente d’assurer une forme
        de rétrocompatibilité et d’interopérabilité.
      </p>

      <p>
        Tant que les acteurs sont divers, tant que le rapport de force est
        largement réparti et équilibré, cette situation peut perdurer. Et la
        nature (relativement) décentralisée du réseau a longtemps laissé croire
        qu’il en serait toujours ainsi. Les pays et les entreprises, aussi
        puissants fussent-ils, n’ont pas encore totalement détraqué cette
        machine plutôt bien huilée, qui rend service jour et nuit à des
        milliards d’individus.
      </p>

      <p>
        Cela ne doit pas non plus nous rendre aveugles. La position centrale de
        certains pays, comme les États-Unis ou la Chine, montrent bien que la
        décentralisation et l’équilibre sont loin d’être parfaits. Et bien sûr,
        comme nous en avons déjà parlé, la position dominante de Microsoft puis
        de Google ont fait trembler à de multiples reprises un bien commun
        encore basé en grande partie sur des technologies ouvertes et
        accessibles.
      </p>

      <p>
        Mais nous sommes légion.
      </p>
      <p>
        Tant que nous utilisons, créons et récupérons des données sur le web,
        avec nos besoins et outils particulièrement variés, nous parviendrons
        peut-être à faire vivre cette diversité longtemps. Certes, Google est
        aujourd’hui dans une situation particulièrement prédatrice, en ayant
        une position très solide à la fois du côté des navigateurs, des OS, des
        sites et de la rémunération en ligne. Mais ses tentatives d’imposer des
        changements radicaux sont rarement acceptées sans une critique acerbe
        et salvatrice des autres acteurs.
      </p>

      <p>
        Nous sommes légion, c’est super. Mais en toute franchise, chacune et
        chacun derrière son écran, on fait quoi ?
      </p>
    </section>

    <section>
      <h3>Faire vivre l’invisible</h3>

      <p>
        Face à des entreprises gigantesques, il serait facile de prétendre que l’on
        ne peut rien faire. C’est pourtant nous qui avons le pouvoir, ensemble.
        C’est à ce moment que nous revenons aux outils invisibles que tout le
        monde utilise sans le savoir.
      </p>

      <p>
        Tout le monde ne peut pas créer des outils utilisés par tout le monde.
        Mais pour continuer à faire vivre ces outils et apporter un contrepoids
        nécessaire, tout le monde peut aider celles et ceux qui créent ces
        outils.
      </p>

      <p>
        Utiliser les outils, de manière consciente, c’est déjà aider. Tant que
        ces outils existent et sont très largement utilisés, il sera difficile
        de faire évoluer le web vers des solutions qui enferment. Bien sûr,
        malgré cela, on voit émerger un nombre croissant de fonctionnalités
        limitées à un navigateur particulier. Mais la pauvreté du choix dans
        les navigateurs ne doit pas s’étendre aux autres utilisations du web.
      </p>

      <p>
        C’est justement parce que les navigateurs ne sont pas les seules portes
        d’entrée que le web reste ouvert. On utilise des API, des scrapers et
        des scripts avec les mêmes protocoles, pour lire les mêmes formats. Et
        tout reste encore à inventer, autant au niveau des outils que des
        usages.
      </p>

      <p>
        Il faut faire fourmiller ces usages. Il ne faut pas avoir peur de
        sortir des sentiers battus et d’aller au-delà de ce qui est prévu. Les
        nouvelles idées basées sur les outils actuels assoient à chaque fois un
        peu plus une diversité que l’on ne peut pas renverser.
      </p>

      <p>
        Au niveau des navigateurs également, où la bataille semble perdue, on
        peut continuer à utiliser autre chose que Chrome. Continuer à faire
        vivre Firefox, même à un niveau très bas, est la seule manière de
        pousser les créateurs de sites à continuer de le tester, et à ne pas se
        laisser happer par les « fonctionnalités » spécifiques du navigateur de
        Google.
      </p>

      <p>
        Enfin, au-delà de l’usage et du développement d’outils variés, il est
        possible d’aider ce qui existe. Comme nous l’avons vu, les « équipes »
        derrière le code sont souvent composées de peu de bénévoles, et toute
        forme de soutien est extrêmement importante pour donner envie aux
        créatrices et aux créateurs de continuer.
      </p>

      <p>
        Être bénévole ne veut pas dire que l’on n’accepte pas d’argent. La
        meilleure manière d’encourager durablement un projet est de payer les
        personnes qui le développent. Le code est librement disponible, mais
        son développement a un coût, ne serait-ce que pour le temps passé. De
        nombreuses solutions de sponsoring et de dons existent, n’hésitez pas à
        les utiliser, même pour des montants relativement faibles.
      </p>

      <p>
        Nous pouvons faire beaucoup de choses. Selon nos moyens, selon notre
        temps et nos capacités, nous pouvons choisir les méthodes que nous
        préférons. L’important est de ne pas oublier les diverses possibilités
        qui nous sont offertes, et d’apporter une estime méritée et nécessaire
        à leur durabilité. Chaque petit geste compte.
      </p>

      <p>
        Ensemble, avec nos différences, nous sommes la diversité du web.
      </p>
    </section>

  </article>

{% endblock content %}
