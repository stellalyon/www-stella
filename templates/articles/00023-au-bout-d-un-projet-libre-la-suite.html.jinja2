{% set title = 'Blog − Au bout d’un projet libre : la suite' %}
{% set description = 'Avoir du code qui fonctionne, ce n’est pas la fin. Bien au contraire… Vous voilà plutôt au début de l’aventure.' %}
{% set image = url_for('static', filename='images/00023-road.jpg') %}

{% extends '_layout.jinja2' %}

{% block content %}

  <article>
    <header>
      <h2>Au bout d’un projet libre : la suite (4 / 4)</h2>
      <aside><time datetime="2020-12-04">4 décembre 2020</time>, par Guillaume</aside>
      <p>
        Avoir du code qui fonctionne, ce n’est pas la fin. Bien au
        contraire… Vous voilà plutôt au début de l’aventure.
      </p>
    </header>

    {% include 'articles/_libre_contents.jinja2' %}

    <section>
      <h3>D’un outil pour soi à un outil pour d’autres</h3>

      <h4>Le premier commentaire</h4>
      <p>
        Au début, nous n’allons pas nous mentir : il n’y aura que
        vous. Personne d’autre pour utiliser, pour tester, pour donner son avis
        sur votre création. Ce n’est pas la peine de faire la tête, profitez
        plutôt de votre tranquillité. Parce que si tout se passe bien, elle ne
        va pas durer.
      </p>
      <p>
        Vous allez peut-être parler de votre logiciel à quelques connaissances,
        des personnes susceptibles d’utiliser votre merveille, ou au moins d’y
        jeter un coup d’œil. Il risque d’y avoir encore quelques semaines,
        quelques mois… Mais si vous continuez à utiliser votre logiciel, et à
        le développer un minimum, arrivera ce qui devait arriver : un
        commentaire.
      </p>
      <p>
        Là, tout dépend des outils que vous avez utilisés. Généralement, vous
        mettrez votre code sur une forge logicielle, avec un minimum de
        métadonnées : un README, une description, quelques étiquettes… La prise
        de contact avec le monde extérieur risque d’arriver sous la forme d’un
        ticket.
      </p>
      <p>
        Un seul conseil : ne paniquez pas.
      </p>

      <h4>La persévérance</h4>
      <figure>
        <img src="{{ url_for('static', filename='images/00023-road.jpg') }}" alt="Photo de route" title="Notre route est droite, mais la pente est forte." />
        <figcaption>
          Facile : c’est tout droit.
        </figcaption>
      </figure>
      <p>
        Cette quête est longue et fastidieuse. Vous aurez peut-être quelques
        personnes qui viendront vous voir au bout de quelques années. Peut-être
        n’est-ce qu’un début. On ne sait pas à l’avance quels projets
        fonctionneront…  L’important est de durer assez pour que les gens
        affluent, ou que le logiciel devienne obsolète.
      </p>
      <p>
        Prenez soin des gens qui viennent. Vous pouvez les bichonner, répondre
        à leurs attentes (parfois floues, souvent discutables), et les
        encourager à donner leur avis. Après quelques messages, on peut parfois
        avoir un effet boule de neige, avec de plus en plus de personnes qui
        réagissent, qui en amènent d’autres, et encore d’autres, et encore
        d’autres…
      </p>
      <p>
        Avec un minimum de réactivité, tout le monde devrait être content, ou
        au moins éclairé sur vos choix. Cette réactivité est dure à tenir,
        surtout après plusieurs mois de calme plat, mais c’est votre meilleure
        alliée pour toucher de nouvelles personnes : quand quelqu’un qui
        utilise votre logiciel est content, il a beaucoup plus de chances
        d’amener vers vous d’autres personnes.
      </p>
      <p>
        Vous courez un marathon. Allez-y doucement, mais avec constance.
      </p>

      <h4>Un minimum de communication</h4>
      <p>
        Vous allez faire des choses sur votre logiciel. C’est évidemment très
        dur de communiquer ce qu’il y a dans votre tête, parce que vous passez
        déjà tellement de temps à développer… Et pourtant, il faut au moins
        s’assurer d’accueillir convenablement les gens de l’extérieur, leur
        donner envie de venir vers vous.
      </p>
      <p>
        Si vous arrivez sur un projet avec juste du code, sans savoir ce que ça
        fait, sans savoir comment ça s’utilise, sans savoir comment prendre
        contact avec l’équipe de développement, vous avez peu de chances de
        vous lancer, beaucoup plus de partir en courant. C’est pareil pour
        tout le monde, c’est pareil pour votre logiciel.
      </p>
      <p>
        Le minimum est d’accueillir les arrivants avec quelques mots
        rassurants : une petite description du but et des fonctionnalités,
        quelques lignes pour savoir comment utiliser le logiciel, un moyen de
        vous joindre. Si vous avez le courage, vous pouvez même publier des
        versions, avec quelques notes présentant les nouveautés.
      </p>
      <p>
        Et bien sûr, si vous vous sentez l’âme communicante, n’hésitez pas à
        créer un petit site, de la documentation poussée, du contenu sur les
        réseaux sociaux… Mais comme pour le reste, assurez-vous de tenir la
        distance : vous courez un marathon. Rien de plus triste qu’un compte
        Twitter avec seulement un message d’ouverture de compte, ou un blog
        avec un seul article qui date d’il y a 5 ans. Mieux vaut faire peu
        régulièrement que beaucoup d’un coup au début sans plus rien après !
      </p>
    </section>

    <section>
      <h3>La communauté</h3>

      <h4>La gentillesse, toujours</h4>
      <p>
        La chose la plus importante que j’ai réalisée au long de mes gestions
        de projets est l’incroyable puissance de gentillesse.
      </p>
      <figure>
        <img src="{{ url_for('static', filename='images/00023-flowers.jpg')}}" alt="Photo d’un bouquet de fleurs" title="💮" />
        <figcaption>
          Cadeau :)
        </figcaption>
      </figure>
      <p>
        Avec le temps, je me suis rendu compte que j’avais beaucoup plus envie
        de rapporter des bugs et donner mon avis aux équipes de développement
        les plus gentilles. Et de la même manière, les gens qui contribuaient
        le plus à mes projets étaient les gens avec qui j’avais de bons
        rapports.
      </p>
      <p>
        Ça paraît simple, mais c’est en réalité compliqué.
      </p>
      <p>
        Être gentil, tout le temps, de manière inconditionnelle, ce n’est pas
        facile. C’est d’autant plus vrai que la méthode d’échange principale
        est souvent l’écrit, avec son potentiel manque de chaleur, ses lacunes
        en communication non-verbale, et les incompréhensions qui vont avec.
      </p>
      <p>
        Être gentil, c’est être poli, accueillir les gens, passer du temps à
        essayer de comprendre leurs besoins. C’est expliquer beaucoup de choses
        dans le détail, à des gens qui n’ont parfois pas du tout le même bagage
        technique que vous. C’est long, et il arrive parfois que ce soit très
        frustrant. Et comme tout le reste, c’est difficile à tenir sur la
        distance.
      </p>
      <p>
        Mais après, les gens sont contents. Et vous aussi.
      </p>
      <p>
        Être gentil, ça ne veut pas dire tout accepter. Vous pouvez refuser des
        fonctionnalités qui ne vous plaisent pas, vous pouvez refuser de passer
        du temps à aider certaines personnes parce que c’est trop long ou
        compliqué. Au début, j’ai été très étonné de voir que les gens étaient
        vraiment reconnaissants lorsque je prenais le temps d’expliquer
        pourquoi une fonctionnalité de m’intéressait pas, même lorsque du temps
        avait été passé pour proposer du code.
      </p>
      <p>
        Être gentil, ça ne veut pas non plus dire tout laisser passer. Avoir un
        code de conduite aide beaucoup à régler les problèmes amenés par des
        personnes énervées, vulgaires, insultantes, ou simplement maladroites.
        On peut dire gentiment qu’un commentaire ne suit pas le code de
        conduite. On peut expliquer gentiment que le comportement n’est pas
        acceptable selon les règles mises en place. Et souvent, répondre avec
        sourire et fermeté à de l’agressivité désamorce rapidement la tension.
      </p>

      <h4>La ligne directrice</h4>
      <p>
        Personne ne peut être dans votre tête.
      </p>
      <p>
        Il est facile, lorsque l’on vient vous voir avec une question déjà
        mille fois posée, de dire que la réponse se trouve dans un ticket ou
        dans la documentation, d’une phrase lapidaire et cruelle. C’est
        tentant.
      </p>
      <p>
        Mais vous pouvez également réfléchir : si mille personnes vous posent
        mille fois la même question, c’est peut-être que le problème vient de
        vous.
      </p>
      <figure>
        <img src="{{ url_for('static', filename='images/00023-xkcd.png')}}" alt="Mandatory Related XKCD™" title="The most ridiculous offender of all is the sudoers man page, which for 15 years has started with a 'quick guide' to EBNF, a system for defining the grammar of a language. 'Don't despair', it says, 'the definitions below are annotated.'" />
        <figcaption>
          Le <a href="https://xkcd.com/1343/">XKCD</a> obligatoire. C’est quand
          même pas compliqué, la réponse à cette question est dans la note de
          bas de page de la préface de la documentation. C’est écrit noir sur
          blanc avec des lettres, quoi. Des lettres normales, hein, que tout le
          monde peut lire. Les gens font jamais, jamais d’efforts…
        </figcaption>
      </figure>
      <p>
        Il est très difficile de faire une bonne documentation, et encore plus
        difficile de faire un logiciel qui n’a pas besoin de documentation.
        Mais c’est votre devoir, si vous voulez avoir des utilisatrices et des
        utilisateurs heureux, d’éviter qu’ils ne se posent des questions, ou du
        moins qu’ils trouvent rapidement leurs réponses.
      </p>
      <p>
        La première des documentations est d’expliquer ce que fait votre
        projet, mais aussi ce qu’il ne fait pas.
      </p>
      <p>
        Avoir une ligne directrice forte, clivante, c’est une bonne manière
        d’attirer à vous des personnes réellement intéressées par votre
        logiciel et par vos idées. Et lorsque cette ligne sera claire et bien
        comprise, vous aurez tout le plaisir de combler de bonheur celles et
        ceux qui utiliseront votre projet.
      </p>
      <p>
        Courir votre marathon nécessitera de faire des choix. Si vous portez
        trop de choses, vous finirez par vous épuiser, et par frustrer tout le
        monde autour. Au contraire, si vous commencez avec des objectifs
        simples et faciles à communiquer, vous irez plus loin, vous attirerez
        d’autres personnes capables de porter de nouvelles choses avec vous.
      </p>

      <h4>Accepter la fin</h4>
      <p>
        Comme tout le reste, votre projet mourra un jour. Autant accepter tout
        de suite cette évidence.
      </p>
      <p>
        Ce n’est pas grave.
      </p>
      <p>
        Lorsque vous en aurez marre, quelle que soit la raison, vous pouvez
        laisser votre projet. Si d’autres personnes le souhaitent, elles
        pourront s’en occuper à votre place, et l’emmèneront là où elles
        voudront.
      </p>
      <p>
        Votre projet, ce n’est pas vous. C’est quelque chose qui peut grandir
        avec tout ce que vous lui donnerez, et pour lequel vous pouvez avoir un
        réel attachement sentimental. Mais à un moment, il faudra accepter de
        le laisser s’envoler avec d’autres, ou de reposer en paix.
      </p>
      <p>
        L’une des beautés de l’informatique, et du développement de logiciel
        libre en particulier, est de donner aux gens la possibilité de créer
        beaucoup de choses avec peu de moyens, et de les partager avec qui
        voudra. Ce n’est pas parce qu’un projet s’arrête que tout s’arrête,
        c’est au contraire la possibilité de faire autre chose.
      </p>
      <p>
        Et si, à la fin, vous vient l’idée que vous avez perdu du temps,
        rendez-vous à l’évidence : ces innombrables heures consumées en cendres
        se sont transformées en expérience, en découvertes, en souvenirs…
      </p>
    </section>
  </article>

{% endblock content %}
