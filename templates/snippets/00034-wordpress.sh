# Téléchargement de WordPress
wp-cli core download --locale="fr_FR"

# Création de la configuration de WordPress
wp-cli config create --dbname="db_name" --dbuser="db_user" --dbpass="db_pass"

# Création de la base de données
wp-cli db create

# Installation basique de WordPress
wp core install --url="example.com" --title="Titre" --admin_user="admin" --admin_password="admin_pass" --admin_email="admin@example.com"
