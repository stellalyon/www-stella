# Installation de l’extension bbpress
wp-cli plugin install bbpress

# Activation
wp-cli plugin activate bbpress

# Désactivation
wp-cli plugin deactivate bbpress

# Mise à jour
wp-cli plugin update bbpress
